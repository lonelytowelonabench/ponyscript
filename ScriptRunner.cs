﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PonyScript
{
    public delegate bool GettingQuestStageDelegate(ScriptRunner runner, string Quest, out int Stage);

    public class ScriptRunner : IDisposable
    {
        public GenderNounSets GenderNouns;

        public Dictionary<string, string> scriptVars = new Dictionary<string, string>();
        internal Dictionary<string, bool> Flags;
        internal Dictionary<string, VarAction> Variables = new Dictionary<string, VarAction>();
        public int Choice { internal get; set; }
        public string[] Choices { get; private set; }

        public string LastNpc { get; set; }

        /// <summary>
        /// root script
        /// </summary>
        readonly Script _originalScript;
        /// <summary>
        /// current script
        /// </summary>
        public Script Script { get { return _currentScript; } }

        Script _currentScript;

        private Stack<Script> _scriptStack = new Stack<Script>(4);
        private Stack<int> _scriptStackPosition = new Stack<int>(4);

        public ScriptRunner(Script script)
        {
            this._originalScript = script;
            _currentScript = script;

            Flags = new Dictionary<string, bool>();
            currentLine = 0;
            Choice = -1;

            scriptVars["$LASTLABEL"] = "BEGINNING";
            scriptVars["$CURRENTLABEL"] = "BEGINNING";

            OnChoicesAvailable += IntChoicesAvailable;

        }

        public int CurrentLine
        {
            get { return currentLine; }
            set { currentLine = value; }
        }
        public int OriginalScriptLine { get { return _currentScript.Lines[currentLine].LineNumber; } }
        int currentLine;

        /// <summary>
        /// cause the script runner to descend into another script
        /// really only used for RunScriptAction
        /// </summary>
        /// <param name="script"></param>
        /// <param name="label"></param>
        internal void DescendToScript(Script script, string label)
        {
            _scriptStack.Push(_currentScript);
            _currentScript = script;
            _scriptStackPosition.Push(currentLine);
            //attempt to jump into the script, if that makes sense
            if (!string.IsNullOrEmpty(label))
            {
                if (!Script.Labels.TryGetValue(label, out currentLine))
                    currentLine = 0;
            }
            else
                currentLine = 0;

        }

        internal bool IsImmediateLineChoices()
        {
            //if (currentLine + 1 < script.Lines.Count)
            //    return script.Lines[currentLine + 1] is ChoiceAction;
            return false;
        }

        public object customArg = null;
        public bool Run(object cArg = null)
        {
            bool shouldRerun = false;
            customArg = cArg;

            do
            {
                if (currentLine < _currentScript.Lines.Count)
                {
                    shouldRerun = _currentScript.Lines[currentLine].Invoke(this, ref currentLine);

                    if (currentLine >= _currentScript.Lines.Count)
                    {
                        if (ScriptFinished())
                        {
                            customArg = null;
                            return false;
                        }
                        //continue running. END label is always end, so shouldRerun will be true anyway
                    }
                }
                else
                {
                    if (ScriptFinished())
                    {
                        customArg = null;
                        return false;
                    }
                    //otherwise continue running, as we didn't actually finish
                    shouldRerun = true;
                }

                if (Debug)
                {
                    LineDebug(_currentScript.Lines[currentLine]);
                    //debug mode prevents things that want to continue running from happening (goto's, etc)
                    shouldRerun = false;
                }
            } while (shouldRerun);

            customArg = null;
            return true;
        }

        /// <summary>
        /// start the scriptrunner back at the beginning line
        /// </summary>
        public void StartOver()
        {
            currentLine = 0;
        }

        void IntChoicesAvailable(object sender, ChoiceArgs c)
        {
            Choices = c.choices;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>true if the root script finished</returns>
        bool ScriptFinished()
        {
            if (_scriptStack.Count > 0)
            {
                //come back out of the script
                _currentScript = _scriptStack.Pop();
                currentLine = _scriptStackPosition.Pop();
                return false;
            }

            if (ScriptFinishedEvent != null)
            {
                ScriptFinishedEvent(this, EventArgs.Empty);
            }
            return true;
        }

        #region Events
        /// <summary>
        /// when an npc talks
        /// </summary>
        public event EventHandler<TalkArgs> OnNPCTalked;
        /// <summary>
        /// when an npc starts a shop
        /// </summary>
        public event EventHandler<ShopArgs> OnShopOpened;
        /// <summary>
        /// when an item is added
        /// </summary>
        public event EventHandler<ItemArgs> OnItemAdded;
        /// <summary>
        /// When an item is removed
        /// </summary>
        public event EventHandler<ItemArgs> OnItemRemoved;
        /// <summary>
        /// When xp is added
        /// </summary>
        public event EventHandler<XpArgs> OnXpAdded;
        /// <summary>
        /// When a skill is given
        /// </summary>
        public event EventHandler<IntsArg> OnSkillAdded;
        /// <summary>
        /// When an item should be spawned
        /// </summary>
        public event EventHandler<ItemSpawnedArg> ItemSpawned;
        /// <summary>
        /// When an Monster should be spawned
        /// </summary>
        public event EventHandler<MobSpawnedArg> MobSpawned;
        /// <summary>
        /// Tell the PC to Grow up into an adult!
        /// </summary>
        public event EventHandler<StringArg> GrowUp;
        /// <summary>
        /// Execute a named action with various parameters
        /// </summary>
        public event EventHandler<ExecActionArgs> ExecAction;
        /// <summary>
        /// When the script should pause/leave.
        /// </summary>
        public event EventHandler<WaitArg> Waited;
        /// <summary>
        /// When the player talks
        /// </summary>
        public event EventHandler<TalkArgs> OnPlayerTalked;
        /// <summary>
        /// When choices are available to the player
        /// </summary>
        public event EventHandler<ChoiceArgs> OnChoicesAvailable;
        /// <summary>
        /// When the script has run out of lines
        /// </summary>
        public event EventHandler ScriptFinishedEvent;

        /// <summary>
        /// Activated when a scene is scheduled to start
        /// </summary>
        public event EventHandler<StringsArg> OnSceneStartActivated;

        /// <summary>
        /// When an Phase becomes active
        /// </summary>
        public event EventHandler<IntsArg> OnPhaseActivated;
        /// <summary>
        /// When an Phase becomes deactived
        /// </summary>
        public event EventHandler<IntsArg> OnPhaseDeactivated;

        /// <summary>
        /// When an objective becomes active
        /// </summary>
        public event EventHandler<StringsArg> OnObjectiveActivated;
        /// <summary>
        /// When an objective is completed
        /// </summary>
        public event EventHandler<StringsArg> OnObjectiveCompleted;
        /// <summary>
        /// When an objective is canceled
        /// </summary>
        public event EventHandler<StringsArg> OnObjectiveCanceled;
        /// <summary>
        /// When a kill objective becomes attached to the player
        /// </summary>
        public event EventHandler<StringsArg> OnKillObjectiveAttached;
        /// <summary>
        /// When a quest is completed
        /// </summary>
        public event EventHandler<StringArg> OnPlaySound;
        /// <summary>
        /// When a quest becomes active
        /// </summary>
        public event EventHandler<StringArg> OnQuestActivated;
        /// <summary>
        /// When a quest is completed
        /// </summary>
        public event EventHandler<StringArg> OnQuestCompleted;
        /// <summary>
        /// When a quest changes stages
        /// </summary>
        public event EventHandler<QuestStageArg> OnQuestStageChanged;
        /// <summary>
        /// When a quest is removed (not completed). Only happens with non-completed quests
        /// </summary>
        public event EventHandler<StringArg> OnQuestRemoved;
        /// <summary>
        /// When a journal entry is added or updated
        /// </summary>
        public event EventHandler<StringsArg> OnJournalEntry;
        /// <summary>
        /// When the script has an action it couldn't correctly parse
        /// </summary>
        public event EventHandler<StringArg> OnScriptErrored;
        /// <summary>
        /// When a marker is added
        /// </summary>
        public event EventHandler<StringArg> OnMarkerAdded;
        /// <summary>
        /// When a marker is removed
        /// </summary>
        public event EventHandler<StringArg> OnMarkerRemoved;
        /// <summary>
        /// When an impulse happens
        /// </summary>
        public event EventHandler<StringsArg> OnImpulsed;

        public event EventHandler<IActionArg> OnDebugLine;

        internal void NPCTalked(string npc, string dialog, Emotions e)
        {
            if (npc != null)
            {
                string alias;
                if (_currentScript.Aliases.TryGetValue(npc, out alias))
                    LastNpc = alias;
                else
                    LastNpc = npc;
            }

            if (OnNPCTalked != null)
                OnNPCTalked(this, new TalkArgs(LastNpc, dialog, e));
        }
        internal void ShopOpened(int iShopID)
        {
            if (OnShopOpened != null)
                OnShopOpened(this, new ShopArgs(iShopID));
        }
        internal void ChoicesMadeAvailable(string[] choices)
        {
            if (OnChoicesAvailable != null)
                OnChoicesAvailable(this, new ChoiceArgs(choices));
        }

        internal void ItemAdded(Inventory item)
        {
            if (OnItemAdded != null)
                OnItemAdded(this, new ItemArgs(item));
        }
        internal void ItemRemoved(Inventory item)
        {
            if (OnItemRemoved != null)
                OnItemRemoved(this, new ItemArgs(item));
        }
        internal void GiveXp(string type, int amount)
        {
            if (OnXpAdded != null)
                OnXpAdded(this, new XpArgs(type, amount));
        }

        internal void GiveSkill(int skillId, int upgradeId)
        {
            if (OnSkillAdded != null)
                OnSkillAdded(this, new IntsArg(new[] { skillId, upgradeId }));
        }

        internal void SpawnItem(string resource, float x, float y, float z, float rx, float ry, float rz, float rw)
        {
            if (ItemSpawned != null)
                ItemSpawned(this, new ItemSpawnedArg(resource, x, y, z, rx, ry, rz, rw));
        }
        internal void SpawnMob(string resource, int level, int minHp, int maxHp, float x, float y, float z, float rx, float ry, float rz, float rw)
        {
            if (MobSpawned != null)
                MobSpawned(this, new MobSpawnedArg(resource, level, minHp, maxHp, x, y, z, rx, ry, rz, rw));
        }

        internal void GrowUpCommand()
        {
            if (GrowUp != null)
                GrowUp(this, null);
        }

        internal void ExecActionCommand(string name, string[] @params)
        {
            if (ExecAction != null)
                ExecAction(this, new ExecActionArgs(name, @params));
        }

        internal void Wait(float waitTime, int nextLine)
        {
            if (Waited != null)
                Waited(this, new WaitArg(waitTime, nextLine));
        }
        internal void PlayerTalked(string line, Emotions emotion)
        {
            if (OnPlayerTalked != null)
                OnPlayerTalked(this, new TalkArgs("", line, emotion));
        }

        internal void StartSceneActivated(string contextString, string sceneName)
        {
            if (OnSceneStartActivated != null)
                OnSceneStartActivated(this, new StringsArg(contextString, sceneName));
        }
        internal void PlaySound(string quest)
        {
            if (OnPlaySound != null)
                OnPlaySound(this, new StringArg(quest));
        }

        internal void PhaseActivated(int[] phases)
        {
            if (OnPhaseActivated != null)
                OnPhaseActivated(this, new IntsArg(phases));
        }
        internal void PhaseDectivated(int[] phases)
        {
            if (OnPhaseDeactivated != null)
                OnPhaseDeactivated(this, new IntsArg(phases));
        }
        internal void ObjectiveActivated(string questId, string objectiveId)
        {
            if (OnObjectiveActivated != null)
                OnObjectiveActivated(this, new StringsArg(questId, objectiveId));
        }
        internal void ObjectiveCompleted(string questId, string objective)
        {
            if (OnObjectiveCompleted != null)
                OnObjectiveCompleted(this, new StringsArg(questId, objective));
        }
        internal void ObjectiveCanceled(string questId, string objective)
        {
            if (OnObjectiveCanceled != null)
                OnObjectiveCanceled(this, new StringsArg(questId, objective));
        }
        internal void KillObjectiveAttached(string questId, string questStageToSet, string objective, string nameOfKill, int totalCount, string scriptToRun, string scriptLabel)
        {
            if (OnKillObjectiveAttached != null)
                OnKillObjectiveAttached(this, new StringsArg(questId, questStageToSet, objective, nameOfKill, totalCount.ToString(), scriptToRun, scriptLabel));
        }
        internal void QuestActivated(string quest)
        {
            if (OnQuestActivated != null)
                OnQuestActivated(this, new StringArg(quest));
        }
        internal void QuestCompleted(string quest)
        {
            if (OnQuestCompleted != null)
                OnQuestCompleted(this, new StringArg(quest));
        }
        internal void QuestStageChanged(string quest, int stage)
        {
            if (OnQuestStageChanged != null)
                OnQuestStageChanged(this, new QuestStageArg(quest, stage));
        }
        internal void QuestStageRemoved(string quest)
        {
            if (OnQuestRemoved != null)
                OnQuestRemoved(this, new StringArg(quest));
        }
        internal void JournalEntryEvent(string quest, string journalEntry)
        {
            if (OnJournalEntry != null)
                OnJournalEntry(this, new StringsArg(quest, journalEntry));
        }
        internal void ScriptErrored(string error)
        {
            if (OnScriptErrored != null)
                OnScriptErrored(this, new StringArg(error));
        }
        internal void MarkerRemoved(string MarkerId)
        {
            if (OnMarkerRemoved != null)
                OnMarkerRemoved(this, new StringArg(MarkerId));
        }
        internal void MarkerAdded(string MarkerId)
        {
            if (OnMarkerAdded != null)
                OnMarkerAdded(this, new StringArg(MarkerId));
        }
        internal void Impulsed(string context, string value)
        {
            if (OnImpulsed != null)
                OnImpulsed(this, new StringsArg(context, value));
        }
        private void LineDebug(IAction action)
        {
            var cond = action as IConditional;
            if (OnDebugLine != null)
                OnDebugLine(this, new IActionArg(action.LineNumber, Script.GetOriginalLine(action.LineNumber), new object[0]));
        }
        #endregion

        #region Getter Events
        public event Func<ScriptRunner, Gender> GettingGender;
        internal Gender PlayerGender()
        {
            return GettingGender(this);
        }
        public event Func<ScriptRunner, string> GettingPlayerName;
        internal string PlayerName()
        {
            return GettingPlayerName(this);
        }
        public event Func<ScriptRunner, Inventory, bool> GettingPlayerHasItemAnywhere;
        internal bool PlayerHasItemAnywhere(Inventory inventory)
        {
            return GettingPlayerHasItemAnywhere(this, inventory);
        }
        public event Func<ScriptRunner, Inventory, bool> GettingPlayerHasItem;
        internal bool PlayerHasItem(Inventory inventory)
        {
            return GettingPlayerHasItem(this, inventory);
        }
        public event Func<ScriptRunner, Inventory, bool> GettingPlayerHasItemEquipped;
        internal bool PlayerHasItemEquipped(Inventory inventory)
        {
            return GettingPlayerHasItemEquipped(this, inventory);
        }
        public event Func<ScriptRunner, int, bool> GettingPlayerHasCutieMark;
        internal bool PlayerHasCutieMark(int CutieMark)
        {
            return GettingPlayerHasCutieMark(this, CutieMark);
        }
        public event Func<ScriptRunner, Race> GettingPlayerRace;
        internal Race PlayerRace()
        {
            return GettingPlayerRace(this);
        }

        public event Func<ScriptRunner, int[], bool> GettingPlayerIsInPhases;
        internal bool PlayerIsInPhase(int[] phases)
        {
            return GettingPlayerIsInPhases(this, phases);
        }

        internal string PlayerRaceString()
        {
            var race = PlayerRace();

            switch (race)
            {
                case Race.Earth:
                    return "earth pony";
                case Race.Pegasus:
                    return "pegasus";
                case Race.Unicorn:
                    return "unicorn";
                default:
                    return "pony";
            }
        }
        public event Func<ScriptRunner, string, string, bool> GettingObjectiveIsActive;
        internal bool PlayerObjectiveIsActive(string questId, string objective)
        {
            return GettingObjectiveIsActive(this, questId, objective);
        }
        public event Func<ScriptRunner, string, string, bool> GettingObjectiveIsComplete;
        internal bool PlayerObjectiveIsComplete(string questId, string objective)
        {
            return GettingObjectiveIsComplete(this, questId, objective);
        }
        public event Func<ScriptRunner, string, bool> GettingQuestIsActive;
        internal bool PlayerQuestIsActive(string Quest)
        {
            return GettingQuestIsActive(this, Quest);
        }
        public event GettingQuestStageDelegate GettingQuestStage;
        internal bool PlayerQuestStage(string Quest, out int stage)
        {
            return GettingQuestStage(this, Quest, out stage);
        }
        public event Func<ScriptRunner, string, bool> GettingQuestCompleted;
        internal bool PlayerCompletedQuest(string Quest)
        {
            return GettingQuestCompleted(this, Quest);
        }

        public event Func<ScriptRunner, string, string[], bool> GettingQueryExec;
        internal bool GetQueryExec(bool exclamation, string name, string[] @params)
        {
            var result = GettingQueryExec(this, name, @params);
            if(exclamation) // Not operator
            {
                result = !result;
            }

            return result;
        }
        #endregion

        public int GetLineForLabel(int lineNumber, string destination)
        {
            int jumpLocation;
            if (Script.Labels.TryGetValue(destination, out jumpLocation))
                return jumpLocation;

            throw new Exception(string.Format("[Script] the label {0} defined on line {1} does not exist in script {2}", destination, lineNumber, Script.Name));
        }

        public void MoveToLabel(string label)
        {
            int jumpLocation;
            if (Script.Labels.TryGetValue(label, out jumpLocation))
            {
                scriptVars["$LASTLABEL"] = scriptVars["$CURRENTLABEL"];
                scriptVars["$CURRENTLABEL"] = label;
                currentLine = jumpLocation;
                return;
            }

            throw new Exception(string.Format("[Script] a call was made to jump to label \"{0}\" in script {1} on line {2}, but it does not exist.", label, Script.Name, currentLine));
        }

        public void Dispose()
        {
            OnItemAdded = null;
            OnItemRemoved = null;
            OnXpAdded = null;
            OnJournalEntry = null;
            OnImpulsed = null;
            OnNPCTalked = null;
            OnPlayerTalked = null;
            OnQuestActivated = null;
            OnQuestCompleted = null;
            OnQuestRemoved = null;
            OnQuestStageChanged = null;
            OnScriptErrored = null;
            ScriptFinishedEvent = null;
            OnShopOpened = null;
        }

        public bool Debug { get; set; }
    }

    public class TalkArgs : EventArgs
    {
        internal TalkArgs(string npc, string line, Emotions emotion)
        {
            this.CharacterName = npc;
            this.Dialog = line;
            this.Emotion = emotion;
        }

        public string CharacterName { get; internal set; }
        public string Dialog { get; internal set; }
        public Emotions Emotion { get; internal set; }
    }

    public class XpArgs : EventArgs
    {
        internal XpArgs(string type, int amount)
        {
            Type = type;
            Amount = amount;
        }

        public string Type { get; internal set; }
        public int Amount { get; internal set; }
    }

    public class ShopArgs : EventArgs
    {
        internal ShopArgs(int iShopID)
        {
            ShopID = iShopID;
        }
        public int ShopID { get; internal set; }
    }

    public class ChoiceArgs : EventArgs
    {
        public string[] choices { get; internal set; }
        internal ChoiceArgs(string[] choices) { this.choices = choices; }
    }

    public class ItemArgs : EventArgs
    {
        public Inventory item { get; internal set; }
        public ItemArgs(Inventory item) { this.item = item; }
    }

    public class StringArg : EventArgs
    {
        public string str { get; internal set; }
        internal StringArg(string str) { this.str = str; }
    }

    public class ExecActionArgs : EventArgs
    {
        public string Name { get; private set; }
        public string[] Params { get; private set; }

        internal ExecActionArgs(string name, string[] @params)
        {
            Name = name;
            Params = @params;
        }
    }

    public class StringsArg : EventArgs
    {
        public string[] strings { get; internal set; }
        internal StringsArg(params string[] args)
        {
            strings = args;
        }
    }
    public class IntsArg : EventArgs
    {
        public int[] ints { get; internal set; }
        internal IntsArg(params int[] args)
        {
            ints = args;
        }
    }

    public class WaitArg : EventArgs
    {
        /// <summary>
        /// time to wait
        /// </summary>
        public float WaitTime { get; private set; }
        /// <summary>
        /// line to resume at
        /// </summary>
        public int NextLine { get; private set; }

        public WaitArg(float waitTime, int nextLine)
        {
            WaitTime = waitTime;
            NextLine = nextLine;
        }
    }

    public class ItemSpawnedArg : EventArgs
    {
        public string Resource { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float Rx { get; private set; }
        public float Ry { get; private set; }
        public float Rz { get; private set; }
        public float Rw { get; private set; }

        public ItemSpawnedArg(string resource, float x, float y, float z, float rx, float ry, float rz, float rw)
        {
            Resource = resource;
            X = x;
            Y = y;
            Z = z;
            Rx = rx;
            Ry = ry;
            Rz = rz;
            Rw = rw;
        }
    }

    public class MobSpawnedArg : EventArgs
    {
        public string Resource { get; private set; }
        public int Level { get; private set; }
        public int MinHP { get; private set; }
        public int MaxHP { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float Rx { get; private set; }
        public float Ry { get; private set; }
        public float Rz { get; private set; }
        public float Rw { get; private set; }

        public MobSpawnedArg(string resource, int level, int minHp, int maxHp, float x, float y, float z, float rx, float ry, float rz, float rw)
        {
            Resource = resource;
            Level = level;
            MinHP = minHp;
            MaxHP = maxHp;
            X = x;
            Y = y;
            Z = z;
            Rx = rx;
            Ry = ry;
            Rz = rz;
            Rw = rw;
        }
    }

    public class QuestStageArg : EventArgs
    {
        public string quest { get; internal set; }
        public int stage { get; internal set; }

        internal QuestStageArg(string quest, int stage) { this.quest = quest; this.stage = stage; }
    }

    public static class ExtensionMethods
    {
        public static int[] FindIndexOfAll<T>(this IEnumerable<T> values, Func<T, bool> func)
        {
            return values.Select((value, index) => func(value) ? index : -1).Where(index => index > -1).ToArray();
        }
    }
}
