﻿using PonyScript;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace PonyScriptTests
{
    
    
    /// <summary>
    ///This is a test class for ScriptRunnerTest and is intended
    ///to contain all ScriptRunnerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ScriptRunnerTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        private void SetupPlayer(ScriptRunner target, string name = "Player", Gender gender = Gender.Filly, Race race = Race.Earth)
        {
            target.GettingPlayerName += (sender) => name;
            target.GettingGender += (sender) => gender;
            target.GettingPlayerRace += (sender) => race;
        }

        #region RunScriptAction tests
        
        /// <summary>
        ///A test for Run
        ///</summary>
        [TestMethod()]
        [DeploymentItem("scripts/Hello World.ponyscript")]
        [DeploymentItem("scripts/RunScriptActionTest.txt")]
        public void RunScriptActionTest()
        {
            Script script = Script.LoadFromFile("RunScriptActionTest.txt");
            
            ScriptRunner target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };
            target.GettingPlayerName += (sender) =>
            {
                return "Player";
            };
            target.GettingGender += (runner) =>
            {
                return Gender.Mare;
            };
            target.GettingPlayerRace += (sender) => Race.Earth;

            object cArg = null;

            target.Run(cArg);
            Assert.AreEqual("Run Script Action Pony", lastTalk.characterName);

            target.Run(cArg);
            AssertHelloWorld(lastTalk);

            target.Run(cArg);
            Assert.AreEqual("Run Script Action Pony", lastTalk.characterName);
            Assert.AreEqual("Pony Joe said Hello World", lastTalk.dialog);

            target.Run(cArg);
            AssertHelloWorld(lastTalk);

            target.Run(cArg);
            Assert.AreEqual("Run Script Action Pony", lastTalk.characterName);
            Assert.AreEqual("And once more", lastTalk.dialog);
        }

        /// <summary>
        /// Test that multiple recursings work
        /// </summary>
        [TestMethod()]
        [DeploymentItem("scripts/Hello World.ponyscript")]
        [DeploymentItem("scripts/RecurseRunAction.txt")]
        [DeploymentItem("scripts/Recurse1.txt")]
        public void RunScriptActionRecurseTest()
        {
            Script script = Script.LoadFromFile("RecurseRunAction.txt");

            ScriptRunner target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };
            target.GettingPlayerName += (sender) =>
            {
                return "Player";
            };
            target.GettingGender += (runner) =>
            {
                return Gender.Mare;
            };
            target.GettingPlayerRace += (sender) => Race.Earth;

            object cArg = null;

            target.Run(cArg);
            
            AssertHelloWorld(lastTalk);
        }

        [TestMethod]
        [DeploymentItem("scripts/RunMissingScript.txt")]
        public void RunMissingScriptActionTest()
        {
            Script script = Script.LoadFromFile("RunMissingScript.txt");

            ScriptRunner target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };
            target.GettingPlayerName += (sender) =>
            {
                return "Player";
            };
            target.GettingGender += (runner) =>
            {
                return Gender.Mare;
            };
            target.GettingPlayerRace += (sender) => Race.Earth;

            object cArg = null;

            target.Run(cArg);

            Assert.AreEqual("Pony Joe", lastTalk.characterName);
            Assert.AreEqual("The script didn't exist", lastTalk.dialog);
        }

        [TestMethod]
        [DeploymentItem("scripts/ChoiceAction.txt")]
        public void ChoiceActionTest()
        {
            var script = Script.LoadFromFile("ChoiceAction.txt");

            var target = new ScriptRunner(script);
            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };

            ChoiceArgs choices = null;
            target.OnChoicesAvailable += (sender, args) =>
            {
                choices = args;
            };
            target.GettingPlayerName += (sender) =>
                {
                    return "Player";
                };
            target.GettingGender += (runner) =>
                {
                    return Gender.Mare;
                };
            target.GettingPlayerRace += (sender) => Race.Earth;


            object cArg = null;

            target.Run(cArg);

            AssertHelloWorld(lastTalk);
            target.Run();
            Assert.IsNotNull(choices);
            choices = null;
            target.Choice = 2;
            target.Run();
            Assert.AreEqual("This is next", lastTalk.dialog);

            target.MoveToLabel("BEGINNING");
            target.Run();
            AssertHelloWorld(lastTalk);
            target.Run();
            Assert.IsNotNull(choices);
            target.Choice = 1;
            choices = null;
            target.Run();
            Assert.AreEqual("This is second label", lastTalk.dialog);
        }

        [TestMethod]
        [DeploymentItem("scripts/ChoiceAction.txt")]
        [DeploymentItem("scripts/LabelsRecurse.txt")]
        public void RecursionLabelsDontCollideTest()
        {
            var script = Script.LoadFromFile("LabelsRecurse.txt");

            var target = new ScriptRunner(script);
            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };

            ChoiceArgs choices = null;
            target.OnChoicesAvailable += (sender, args) =>
            {
                choices = args;
            };

            var didFinish = false;
            target.ScriptFinishedEvent += (sender, args) =>
            {
                didFinish = true;
            };
            
            target.GettingPlayerName += (sender) =>
            {
                return "Player";
            };
            target.GettingGender += (runner) =>
            {
                return Gender.Mare;
            };
            target.GettingPlayerRace += (sender) => Race.Earth;


            object cArg = null;

            target.Run(cArg);

            AssertHelloWorld(lastTalk);
            target.Run();
            Assert.AreEqual("This is label 2 in labelsrecurse", lastTalk.dialog);
            //beginning recurse
            target.Run();
            AssertHelloWorld(lastTalk);
            target.MoveToLabel("FirstLabel");
            target.Run();
            Assert.AreEqual("This is first label", lastTalk.dialog);
            target.MoveToLabel("END");
            target.Run();
            Assert.IsTrue(didFinish);
        }

        [TestMethod]
        [DeploymentItem("scripts/HasItem.txt")]
        public void HasItemTest()
        {
            var script = Script.LoadFromFile("HasItem.txt");
            var target = new ScriptRunner(script);
            Dictionary<int, int> items = new Dictionary<int, int>();

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };
            target.GettingPlayerName += (sender) =>
            {
                return "Player";
            };
            target.GettingGender += (runner) =>
            {
                return Gender.Mare;
            };
            target.GettingPlayerRace += (sender) => Race.Earth;

            target.OnItemAdded += (sender, args) =>
                {
                    if (items.ContainsKey(args.item.item))
                        items[args.item.item] += args.item.quantity;
                    else
                        items[args.item.item] = args.item.quantity;
                    
                };
            target.GettingPlayerHasItem += (runner, inventory) =>
                {
                    int quant;
                    if (items.TryGetValue(inventory.item, out quant))
                    {
                        return inventory.quantity <= quant;
                    }
                    return false;
                };

            target.Run();
            AssertHelloWorld(lastTalk);
            lastTalk = null;
            target.Run();
            AssertHelloWorld(lastTalk);
            lastTalk = null;
            var moreLines = target.Run();
            Assert.IsFalse(moreLines);
            Assert.IsNull(lastTalk);
        }

        #endregion

        [TestMethod]
        public void HasItemEquippedTest()
        {
            var script = Script.Load(@"
?hasitemequipped? <1> :hasitem
*goto END
[hasitem]
Pony Joe: Has item equipped
");

            var target = new ScriptRunner(script);

            target.GettingPlayerHasItemEquipped += (sender, inv) =>
            {
                if (inv.item == 1)
                    return true;
                return false;
            };

            string lastTalk = null;
            target.OnNPCTalked += (sender, talk) => lastTalk = talk.dialog;

            SetupPlayer(target);

            target.Run();
            Assert.AreEqual("Has item equipped", lastTalk);
        }

        [TestMethod]
        public void MultiORTest()
        {
            var script = Script.Load(@"
? hasitemequipped? <1> | isage? <Filly> : AllTrue
*goto END
[AllTrue]
Pony Joe: All True
");
            var target = new ScriptRunner(script);

            target.GettingPlayerHasItemEquipped += (sender, inv) =>
            {
                if (inv.item == 1)
                    return true;
                return false;
            };

            string lastTalk = null;
            target.OnNPCTalked += (sender, talk) => lastTalk = talk.dialog;

            SetupPlayer(target);

            target.Run();

            Assert.AreEqual("All True", lastTalk);

        }

        [TestMethod()]
        public void DefaultGenderNounsTest()
        {
            var script = Script.Load(@"
Pony Joe: Hello, $PLAYERGENDER foo
Pony Joe: Hello, $PLAYERTITLE foo
Pony Joe: Hello, $Playergender foo
");
            var target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };

            target.GettingPlayerName += (sender) => "Player";
            target.GettingGender += (sender) => Gender.Filly;
            target.GettingPlayerRace += (sender) => Race.Earth;

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello, filly foo");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello, miss foo");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello, Filly foo");
        }

        [TestMethod()]
        public void UndefinedNameTest()
        {
            var script = Script.Load(@"
*aliasname <Bob> <Pony Bob>
Pony Joe: Hello a
Hello b
Bob: Hello c
Hello d
");
            var target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };

            target.GettingPlayerName += (sender) => "Player";
            target.GettingGender += (sender) => Gender.Filly;
            target.GettingPlayerRace += (sender) => Race.Earth;

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello a");
            Assert.AreEqual(lastTalk.characterName, "Pony Joe");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello b");
            Assert.AreEqual(lastTalk.characterName, "Pony Joe");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello c");
            Assert.AreEqual(lastTalk.characterName, "Pony Bob");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello d");
            Assert.AreEqual(lastTalk.characterName, "Pony Bob");
        }

        [TestMethod]
        public void QueryExecTest()
        {
            var script = Script.Load(@"
? f? FooBar(alsdkjf) : OtherLabel
Pony Joe: Hello a
[OtherLabel]
");

            var target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
            };

            string lastName = null;
            string[] lastArgs = null;
            target.GettingQueryExec += (sender, name, args) =>
            {
                lastName = name;
                lastArgs = args;

                return true;
            };

            target.Run();
            Assert.AreEqual("FooBar", lastName);
            Assert.AreEqual(1, lastArgs.Length);
            Assert.IsNull(lastTalk);
        }



        [TestMethod()]
        [DeploymentItem("scripts/AliasTest.txt")]
        [DeploymentItem("scripts/NoNameTest.txt")]
        [DeploymentItem("scripts/RecurseAliasTest.txt")]
        public void RecurseAliasTest()
        {
            var script = Script.LoadFromFile("RecurseAliasTest.txt");
            var target = new ScriptRunner(script);

            TalkArgs lastTalk = null;
            target.OnNPCTalked += (sender, talk) =>
            {
                lastTalk = talk;
                Debug.WriteLine($"{talk.characterName} ({talk.emotion}): {talk.dialog}");
            };

            target.GettingPlayerName += (sender) => "Player";
            target.GettingGender += (sender) => Gender.Filly;
            target.GettingPlayerRace += (sender) => Race.Earth;

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello");
            Assert.AreEqual(lastTalk.characterName, "PJ");

            //first line in AliasTest
            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello");
            Assert.AreEqual(lastTalk.characterName, "Pony Joe");

            //second line in AliasTest
            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello");
            Assert.AreEqual(lastTalk.characterName, "Pony Joe");
            
            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello");
            Assert.AreEqual(lastTalk.characterName, "PJ");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello");
            Assert.AreEqual(lastTalk.characterName, "Pony Bob");

            target.Run();
            Assert.AreEqual(lastTalk.dialog, "Hello");
            Assert.AreEqual(lastTalk.characterName, "Pony Bob");
        }

        void AssertHelloWorld(TalkArgs t)
        {
            Assert.AreEqual("Pony Joe", t.characterName);
            Assert.AreEqual("Hello World", t.dialog);
        }
    }
}
