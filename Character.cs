﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace PonyScript
{
    public class Character
    {
        public string name = "";
        public int[] cutiemarks = new int[3];
        public List<Inventory> inventory = new List<Inventory>();
        public Dictionary<string, int> ActiveQuests = new Dictionary<string, int>();
        public HashSet<string> CompleteQuests = new HashSet<string>();
        public Dictionary<string, string> JournalEntries = new Dictionary<string, string>();
        public Race race = Race.None;
        public Gender genderAndAge = Gender.Mare;
    }

    public sealed class Inventory
    {
        public int item;
        public int quantity;

        public Inventory() { }
        
        public Inventory(int item, int quantity)
        {
            this.item = item;
            this.quantity = quantity;
        }
    }

    public enum Gender : byte
    {
        Filly,
        Colt,
        Mare,
        Stallion,
    }

    public class GenderNounSet : Dictionary<Gender, string>
    {
    }

    public class GenderNounSets : Dictionary<string, GenderNounSet>
    {
        /// <summary>
        /// Get a new default set
        /// </summary>
        /// <returns></returns>
        public static GenderNounSets Default() => new GenderNounSets
        {
            { "$PLAYERGENDER",
                new GenderNounSet
                {
                    {Gender.Filly,      "filly"     },
                    {Gender.Colt,       "colt"      },
                    {Gender.Mare,       "mare"      },
                    {Gender.Stallion,   "stallion"  }
                }
            },
            { "$PLAYERTITLE",
                new GenderNounSet
                {
                    {Gender.Filly,      "miss"      },
                    {Gender.Colt,       "sir"       },
                    {Gender.Mare,       "ma'am"     },
                    {Gender.Stallion,   "sir"       }
                }
            },
            { "$PLAYERPRONOUN",
                new GenderNounSet
                {
                    {Gender.Filly,      "her"       },
                    {Gender.Colt,       "his"       },
                    {Gender.Mare,       "her"       },
                    {Gender.Stallion,   "his"       }
                }
            },
        };

        internal static GenderNounSets DefaultInstance = Default();
    }
}
