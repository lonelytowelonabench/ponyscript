﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PonyScript
{
    public class Enum
    {
        public static bool TryParse<T>(string value, bool ignoreCase, out T parseValue) where T : struct
        {
            try
            {
                if (value == "")
                {
                    parseValue = default(T);
                    return false;
                }
                parseValue = (T)System.Enum.Parse(typeof(T), value, ignoreCase);
                return true;
            }
            catch
            {
                parseValue = default(T);
                return false;
            }
        }
    }
}
