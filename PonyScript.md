PonyScript
==========

PonyScript is a scripting language developed exclusively for PonyQuest to make crafting adventure game-type stories easy and efficient. It is intended to be very easy to pick up, regardless of your skill as a programmer.


Comments
--------

Comments begin with a pound symbol (#) and continue until the end of the line. Comments must stand on their own line.

    # this is just for your reference, and won't be visible to the player

Comments have no effect on the script at all when it comes time to actually process it. They're just to help you out, remind of stuff, organize your scripts a little bit, and so on.


Dialogue
--------

Dialogue is the basic building block of PonyScript. Why? Well, when you're making an adventure game, pretty much the most common scripted thing that happens is a character saying something. Whether it's your player character commenting on something in her environment or an NPC insulting your mother, dialogue happens all the time. Thankfully, in PonyScript, it's super simple to write:

    Twilight Sparkle: All the ponies in this town are CRAZY!

See? Really easy. Just put the character's name, a colon, and then what you want them to say. No quotes needed, no crazy syntax. It's easy.

You might be realizing right about now how much of a pain it's going to be to type out "Twilight Sparkle" every time you want the eponymous pony (sorry, couldn't resist) to say something. You're right, that would suck! Thankfully, PonyQuest supports defining "short names" for characters. You could call Twilight Sparkle "Twi," "TD," or even just "T" if you really wanted to.

One more thing: to cut back on retyping character names repeatedly even further, you can omit the character name entirely if the same character is saying two lines in a row, like this:

    Pinkie Pie: She's an evil enchantress!
                She does evil dances!
                And if you look deep in her eyes...
                ...She'll put you in trances!

As you can see, it looks nicer if you indent the subsequent lines to match the first one, but this isn't necessary.


### Reserved character names

When you write dialogue with these special character names, special things happen:

* `NARRATOR` - makes this line a line of narration instead of dialogue, see "Narration," below
* `PLAYER_CHARACTER` - sets the speaker of this line to the current player character, whomever that may be


Dialogue with emotion
---------------------

Probably the biggest problem with translating *My Little Pony: Friendship is Magic* into a video game is trying to capture the feeling of the fantastic animation that the show uses. If you were to just use dialogue as shown above, your story would look pretty boring: the ponies would just be talking back and forth with their default expressions, not emoting at all.

This is where "emotion dialogue" comes in. The syntax is really easy to learn, because it's just like regular dialogue, but you throw in an emotion between parenthesis before the colon, like this:

    Twilight Sparkle (angry): You have GOT to be kidding me!

Note that you're limited to using the emotions defined in the sprite of the character you're referencing. If the sprite doesn't have an animation for that emotion (in this example, a `talk_angry` animation), then the dialogue will just appear as normal.

valid emotions (not case sensitive)

none,
neutral,
joy,
pleased,
happy,
angry,
sad,
disgusted,
surprised,
confused,
bored,
fearful,
fear,
afraid,
tired,
mischievous,
irritated,
proud,
drunk,
hopeful,
guilty,
shocked,
covetous,
greedy,
paralyzed,
apples,
sick,
disgusted,
derp,
excited,
flirty,
silly,
serious,
blank,
incredulous,
confident,
pouty,
despondent,
sarcastic,
crazed,

Narration
---------

Narration is even easier than dialogue! Really, all you do is write the dialogue normally, but without any name before the colon. PonyScript will interpret this as being narration, and display it in-game as such. You can even have multiple lines of narration, just like dialogue.

    : It was a dark and stormy night in Ponyville...
      ...but only because Rainbow Dash dozed off early.

Also, super technically, you can write narration like regular dialogue and use the reserved character name `NARRATOR` (`NARRATOR: This be narration!`), but why would you do that when I went out of my way to purposefully make an easy-to-use syntax to make your life easier?


Labels
------

Labels are the easiest way to control script logic flow. Just put some characters (no spaces) between some square brackets, and bam, you've got yourself a label:

    [PinkiePieStartsSinging]

Labels are exactly what they sound like: they label things. Think of labels like bookmarks, or notes written in your high school textbook in pen. They let you refer back to a part in the script by name, in case you need to go back to it. More on this in the next section.


### Reserved labels

PonyScript defines a few labels for you, to make things easy. For example, it's pretty common to reference the beginning and end of the script, especially when you want the script processor to jump there (don't worry about what this means, we'll get to it in a second).

The following labels are built into PonyScript, and you'll get an error if you try to define a label with the same name:

* `BEGINNING` - beginning of the script
* `END` - end of the script


Goto
----

So now you know how to set up labels to reference parts of the script, but now you're wondering what good that'll do you at all. Not much, without goto statements!

As you probably assumed, PonyScript scripts are processed sequentially; that is, if your script is just three lines of dialogue, and you run it, it'll do those three lines of dialgue in order. Labels and goto statements let you make more complex scripts that can branch off in different ways, repeat parts over and over again, and generally make your script feel less... uh, scripted.

A goto statement is made by putting an asterisk at the beginning of the line, followed by a space, then the word "goto," then another space, then the label you want to... go to. Like this:

    * goto PinkiePieStartsSinging

See? Not too hard. The script will stop executing sequentially when it reaches a goto statement, and "jump" forwards or backwards to the label it refers to. Consider the following script:

    Pinkie Pie: I'm being super annoying right now!
                No, seriously!
    [SuperAnnoying]
    Pinkie Pie: Super, SUPER annoying!
    * goto SuperAnnoying

Point your finger at the first line, read it (aloud), then move your finger down a line, then read the second line (aloud [no, seriously]). Move your finger down to the third line, but skip it, as it's just a label and not any dialogue or anything that'll visibly impact the game. Now move your finger to the fourth line and read it (aloud [you think I'm joking?]). Then move your finger to the fifth line, and... ah! A goto statement! Move your finger to the line where you see the label its referring to (this may or may not be an IQ test).

This is how PonyScript reads scripts. It is important that you understand this in order to create complex scripts of your own.

As a quick note: you're going to see many (in fact, most) other PonyScript "statements" beginning just like this one: asterisk, space, statement type. Why is this, you ask? Well, since I was nice and made it super easy to write multiple lines of dialgue in a row from the same speaking character (remember?), and those lines just start with letters (or maybe numbers), there has to be some way of differentiating between dialogue and script statements. Hence, the asterisk.


Dialogue choices
----------------

You might've noticed something about that last little excercise we did: we essentially created an infinte loop. The script processor (your finger) kept reading the same line of dialogue over and over again, because it kept going from the dialogue line to the goto statement back to the label then back to the dialogue line, and so on and so forth, *FOREVER!*

So maybe you're thinking that goto statements are stupid and I'm dumb for including them as a feature of PonyScript.

Not so!

It *is* true that with *just* labels and goto statements, there's not a lot we can do, but that's where dialogue choices come in.

Dialogue choices are the easist way to make your script "branch" off into different directions. They're also really easy to understand: odds are you've played a game where a character asked you a question and the game gave you options as to how to answer.

Well, like I said, it's easy in PonyScript: all you do to make a regular line of dialogue into a line of dialogue with choices is:

1. Indent with two spaces (on a new line)
2. Put a greater-than sign (>)
3. Put a space
4. Write the answer as the player should see it
5. Put a space
6. Put a colon and then without putting a space, the label to go to, should the player choose this choice

That probably sounds pretty complicated! It's not, though. Seriously. Let me show you an example of a pretty good chunk of script, utilizing everything we've learned so far:

    Rainbow Dash: Hey, Twilight!
                  What's up?
    Twilight Sparkle: Oh, not too much, Rainbow Dash.
                      I'm just looking for something interesting to do for this demo.
    Rainbow Dash: Hey, me too! Let's do something!

    [RDQuestion]
      > We could go see what Fluttershy's up to! :RainbowJerk
      > Why don't we visit Pinkie Pie? :RainbowJerk
      > I haven't talked to Rarity in awhile... we could go see her. :RainbowJerk
      > ...I could just watch you practice flying... :PrepareToPractice

    Pinkie Pie: You'll never see THIS line of dialogue! Ahahahahaha!

    [RainbowJerk]
    Rainbow Dash: Ughhh...
    Rainbow Dash: Yeah, as fun as THAT sounds, why don't we try doing something actually, y'know, FUN?
    * goto RDQuestion

    [PrepareToPractice]
    Rainbow Dash: Now THAT'S what I'm talking about!
                  Let's go!
    : And then they went off and had fun and whatnot. The end.

...Okay, yeah, that still looks kind of complicated. Let's break it down.

First, we have a label called "RDQuestion." Then Rainbow Dash asks you, the player, what you want to do today. You are presented with four options, three of which involve other ponies and one which involves doing what Rainbow Dash wants to do.

When you, the player, picks one of the presented choices, the game then takes you to the label after the choice (after the colon [no, the player doesn't see the choice {or the colon}]). As you can see, Rainbow Dash is being a bit of a jerk today; choosing anything other than what she obviously already has in mind to do will send you to the "RainbowJerk" label, where she subtly lets you know that your choice wasn't the once she wanted you to pick, then forces you to pick again.

See? This is what I was talking about when I said that goto statements had a use other than causing infinite insanity loops. Here, we use sort of mini-goto statements embedded in the dialogue choices (because that's more or less what they really are), and we even use a regular goto statement to send you back to the beginning of the question if you choose "incorrectly."

And *then*, if you *do* choose the "right" choice, it takes you to a *different* label: "PrepareToPractice," which presumably continues the story.

Note that you can have multiple choices link to the same label; this is typically used for comedic effect.

Also note the typically fourth-wall breaking Pinkie Pie line. Since there is no way out of a dialogue choice other than linking to a label, anything immediately succeeding one will never be seen by the player. Just try the pointing-your-finger-at-the-line-and-reading-aloud thing again; no matter which choice you make, you'll never see the Pinkie Pie line. This is important to note, so you don't accidentally make portions of your script unable to be seen by the player.


### Action choices

When the player selects a choice from the list, the script parser assumes that this is dialogue that the player character is supposed to say. In the above example, maybe you want to allow the player to disregard Rainbow Dash entirely, as she's being kind of a jerk. In that case, just add another choice, but put the text of the choice in square brackets, like this:

      > [Walk away] :END

This tells the parser that this is an "action" choice, or something that your player character is supposed to do instead of say out loud. Note that the square brackets *will* appear in the game on the choice list; this is intentional, to let the player know that the choice is an action choice.


Giveitem
--------

[TODO]

    * giveitem "Yellow Balloon"


Takeitem
--------

[TODO]

    * takeitem "Yellow Balloon"


Emote
-----

[TODO]

    * emote <Twilight Sparkle> angry


Face
----

[TODO]

    * face <Rainbow Dash> E


~~Displayname~~
-----------

[TODO]

    * displayname <door> open door


~~Verb~~
----

[TODO]

    * verb <door> open


~~Move~~
----

[TODO]

    * move "Rainbow Dash" to (320, 240)
    * move "Rainbow Dash" up 42


~~Animate~~
-------

[TODO]

    * animate "Pinkie Pie" deflatehair


~~Changesprite~~
------------

[TODO]

    * changesprite "Pinkie Pie" "takua108/MoarPonies/Pinkamina"


~~Setidle~~
-------

[TODO]

    * setidle "Rainbow Dash" flying_idle


~~Enter~~
-----

[TODO]

    * enter "Pinkie Pie" (-128, 240)
    * move "Pinkie Pie" left 256


~~Exit~~
----

[TODO]

    * move "Pinkie Pie" to (768, 240)
    # because the width of the screen is 640, 768 is 128 pixels "off screen"
    * exit "Pinkie Pie"


~~Together~~
--------

[TODO]

    [TODO]


~~Setplayercharacter~~
------------------

[TODO]

    [TODO]


~~Stage~~
-----

[TODO]

    [TODO]


Flag
----

[TODO]

    * flag DoorToHouseOpen


SetFlag
-------

[TODO]

    * setflag DoorToHouseOpen true
    * setflag DoorToHouseOpen on
    * setflag DoorToHouseOpen 1
    * setflag DoorToHouseOpen false
    * setflag DoorToHouseOpen off
    * setflag DoorToHouseOpen 0


Conditionals
------------

[TODO]

### Hasitem

[TODO]

    * hasitem? <Yellow Balloon> :WantsYourBalloon

### Flagset

[TODO]

    * flagset? <DoorToHouseOpen> :BetterGoCloseIt

### ~~Playercharacteris~~

[TODO]

    [TODO]