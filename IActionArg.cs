﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PonyScript
{
    public class IActionArg : EventArgs
    {
        public int LineNumber { get; set; }
        public string Line { get; set; }
        public object[] Values { get; set; }

        public IActionArg(int lineNumber, string line, object[] values)
        {
            LineNumber = lineNumber;
            Line = line;
            Values = values;
        }
    }
}
