﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;


namespace PonyScript
{
    public class AliasAction : IAction
    {
        public int LineNumber { get; set; }
        public string CharacterToAlias;
        public string NewAlias;

        public AliasAction(string characterToAlias, string newAlias)
        {
            CharacterToAlias = characterToAlias;
            NewAlias = newAlias;
        }

        public bool Invoke(ScriptRunner sr, ref int n)
        {
            return false;
        }
    }

    public class DialogueAction : IAction
    {
        static System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

        public int LineNumber { get; set; }
        public string DialogIdentifier;
        public string Character;
        public string Line;
        public Emotions Emotion;
        
        public DialogueAction(string character, string line, string emotion, int lineNum)
        {
            Character = character != null ? character.Trim() : null;

            if (!Enum.TryParse<Emotions>(emotion, true, out Emotion))
            {
                Emotion = Emotions.none;
                if (emotion != string.Empty)
                    Console.WriteLine("Could not parse emotion {0}. line {1}", emotion, lineNum);
            }
            LineNumber = lineNum;
            Line = line;
        }

        public DialogueAction(string character, string line, string emotion, string dialogId, int lineNum)
        {
            Character = character != null ? character.Trim() : null;

            if (!Enum.TryParse<Emotions>(emotion, true, out Emotion))
            {
                Emotion = Emotions.none;
                if (emotion != string.Empty)
                    Console.WriteLine("Could not parse emotion {0}. line {1}", emotion, lineNum);
            }
            LineNumber = lineNum;
            Line = line;
            DialogIdentifier = dialogId;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            var nLine = Line.Replace("$PLAYERNAME", myTI.ToTitleCase(script.PlayerName()));
            var pgender = script.PlayerGender();
            var gender = pgender.ToString();
            var race = script.PlayerRaceString();

            foreach (var set in script.GenderNouns ?? GenderNounSets.DefaultInstance)
            {
                string genderWord;
                if (!set.Value.TryGetValue(pgender, out genderWord))
                    genderWord = "UNKGEN " + gender + " FOR " + set.Key;

                nLine = nLine.Replace(set.Key, genderWord);

                var titleKey = myTI.ToTitleCase(set.Key.ToLowerInvariant()); //because they were probably defined as all upper, and that doesn't work with ToTitleCase
                nLine = nLine.Replace(titleKey, myTI.ToTitleCase(genderWord));
            }
            
            nLine = nLine.Replace("$PLAYERRACE", race);
            nLine = nLine.Replace("$Playerrace", UppercaseFirst(race));
            //nLine = nLine.Replace("$MARKOV", (new System.Net.WebClient()).DownloadString("http://luna.thehorseplace.us/derpyy/derpyy.php?action=getMarkov&server=irc.irchighway.net&channel=%23legendsofequestria&seed=hello&length=20"));

            string varendchars = " ~!@#$%^&*()+`-={}|[]\\:\";'<>?,./";

            for (int i = 0; i < nLine.Length; ++i)
            {
                try
                {
                    if (nLine[i] == '$')
                    {
                        int endvar = 1;
                        while (!varendchars.Contains(nLine[i + endvar]) && i + endvar < nLine.Length)
                            endvar++;

                        string repkey = nLine.Substring(i, endvar);

                        string repvar = "(UNDEFINED VARIABLE " + repkey + ", REPORT THIS)";
                        if (script.scriptVars.ContainsKey(repkey))
                            repvar = script.scriptVars[repkey];

                        nLine = nLine.Substring(0, i) + repvar + nLine.Substring(i + endvar);
                        i += repvar.Length - 1;
                    }
                }
                catch (Exception ex) { }
            }



            nLine = nLine.Replace("{", "").Replace("}", "");
            script.NPCTalked(Character, nLine, Emotion);
            ++currentLine;
            return script.IsImmediateLineChoices();
        }

        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }

    public class VarConditionalAction : IAction
    {
        public int LineNumber { get; set; }

        string val1;
        string val2;
        string varCondLine;

        public VarConditionalAction(string iVarCondLine)
        {
            varCondLine = iVarCondLine;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            /* Usually, these would be parsed before getting in here.
             * However, strangely, if I parse these at any point before
             * right here, they get stuck to old values - if it started off as 
             * var 1 being something, it would stay that forever, making 
             * random jumping not work right.
             * If someone can parse them at the proper place,
             * that'd be good.
             */

            val1 = varCondLine.Split('=')[0].Substring(5);
            if (val1.EndsWith(" "))
                val1 = val1.Substring(0, val1.Length - 1);

            string linehalf2 = varCondLine.Split('=')[1].Substring(1);
            string lbl = linehalf2.Split(':')[1];
            if (lbl.EndsWith(" "))
                lbl = lbl.Substring(0, lbl.Length - 1);
            if (lbl.StartsWith(" "))
                lbl = lbl.Substring(1);

            val2 = linehalf2.Split(':')[0];
            if (val2.StartsWith(" "))
                val2 = val2.Substring(1);
            if (val2.EndsWith(" "))
                val2 = val2.Substring(0, val2.Length - 1);

            if (val1.StartsWith("$"))
            {
                if (script.scriptVars.ContainsKey(val1))
                    val1 = script.scriptVars[val1];
                else
                    val1 = "(UNDEFINED VARIABLE " + val1 + ", REPORT THIS)";
            }
            if (val2.StartsWith("$"))
            {
                if (script.scriptVars.ContainsKey(val2))
                    val2 = script.scriptVars[val2];
                else
                    val2 = "(UNDEFINED VARIABLE " + val2 + ", REPORT THIS)";
            }
            currentLine++;

            if (val1 == val2)
                script.MoveToLabel(lbl);

            return true;
        }
    }

    public class ChoiceAction : IAction
    {
        public const string NEXT_LINE = "NEXT";
        public readonly IConditional Conditional;
        public int LineNumber { get; set; }
        public string Line;
        public string Destination;
        public bool Dialogue;
        public Emotions Emotion;

        public ChoiceAction(string line, string destination, bool dialogue, int lineNum)
        {
            Line = line;
            Destination = destination.Trim();
            Dialogue = dialogue;
            LineNumber = lineNum;
            Emotion = Emotions.none;
        }

        public ChoiceAction(string line, string emotion, string destination, bool dialogue, int lineNum)
        {
            Line = line;
            Destination = destination.Trim();
            Dialogue = dialogue;
            LineNumber = lineNum;
            if (!Enum.TryParse<Emotions>(emotion, true, out Emotion))
                Emotion = Emotions.none;
        }

        public ChoiceAction(string line, IConditional conditional, string destination, bool dialogue, int lineNum)
            :this(line, destination, dialogue, lineNum)
        {
            Conditional = conditional;
        }

        static System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;
        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            List<ChoiceAction> choices = new List<ChoiceAction>();
            List<string> choiceLines = new List<string>();

            int i = 0;
            while (script.Script.Lines[currentLine + i] is ChoiceAction)
            {
                var choice = (ChoiceAction) script.Script.Lines[currentLine + i];
                if (choice.Conditional == null || choice.Conditional.Query(script))
                {
                    choices.Add(choice);
                    var nLine = choice.Line.Replace("$PLAYERNAME", myTI.ToTitleCase(script.PlayerName()));
                    var gender = script.PlayerGender().ToString();
                    var race = script.PlayerRaceString();
                    nLine = nLine.Replace("$PLAYERGENDER", gender.ToLower());
                    nLine = nLine.Replace("$Playergender", myTI.ToTitleCase(gender));
                    nLine = nLine.Replace("$PLAYERRACE", race);
                    nLine = nLine.Replace("$Playerrace", DialogueAction.UppercaseFirst(race));
                    choiceLines.Add(nLine);
                }
                i++;
            }

            if (script.Choice == -1)
            {
                script.ChoicesMadeAvailable(choiceLines.ToArray());
            }
            else if (script.Choice >= choices.Count)
            {
                script.ChoicesMadeAvailable(choiceLines.ToArray());
            }
            else
            {
                if (choices[script.Choice].Dialogue) 
                    script.PlayerTalked(choiceLines[script.Choice], choices[script.Choice].Emotion);

                if (choices[script.Choice].Destination == NEXT_LINE)
                {
                    //fall through to after the choices in the script
                    //prevents from needing labels if you're just doing back and forth dialog with no choices
                    currentLine += choices.Count;
                }
                else
                    script.MoveToLabel(choices[script.Choice].Destination);
                
                script.Choice = -1;
                return true;
            }

            return false;
        }
    }

    public class LabelAction : IAction
    {
        public int LineNumber { get; set; }
        public string Name;

        public LabelAction(string name, int lineNum)
        {
            Name = name.Trim();
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            //labels do nothing
            ++currentLine;
            return true;
        }
    }

    public class RunScriptAction : IAction
    {
        public string ScriptPath;
        public Script Script;
        public string GotoLabel;
        public int LineNumber { get; set; }

        public RunScriptAction(string scriptPath, int lineNum)
        {
            ScriptPath = scriptPath.Trim();
            LineNumber = lineNum;
        }

        public RunScriptAction(string scriptPath, string gotoLabel, int lineNum)
        {
            ScriptPath = scriptPath.Trim();
            LineNumber = lineNum;
            GotoLabel = gotoLabel.Trim();
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            //cause the script to move to the next position before descending
            ++currentLine;
            if (Script != null)
                //descend. When the script pops back out, it'll be on the next line
            {
                script.DescendToScript(Script, GotoLabel == null ? null : GotoLabel.Replace("$LASTLABEL", script.scriptVars["$LASTLABEL"]).Replace("$CURRENTLABEL", script.scriptVars["$CURRENTLABEL"]));
            }
            return true; //to start executing the descended script
        }

        internal void LoadScript(string nScript, bool useCache = true)
        {
            Script = Script.LoadFromFile(nScript, useCache);
        }
    }

    public class ShopAction : IAction
    {
        public int LineNumber { get; set; }
        public int ShopID;
        public ShopAction(string iLine)
        {
            var tmpSplit = iLine.Split(' ');
            try
            {
                int.TryParse(tmpSplit[2], out ShopID);
            }
            catch (Exception ex)
            {
                ShopID = -1;
            }
        }
        public ShopAction(int iShopID)
        {
            ShopID = iShopID;
        }
        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ShopOpened(ShopID);
            ++currentLine;
            return true;
        }
    }

    public class VarSetAction : IAction
    {
        public int LineNumber { get; set; }

        public string variable = "";
        public string contents = "";

        public VarSetAction(string ivar, string icontents)
        {
            variable = ivar;
            contents = icontents;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (contents.StartsWith("random("))
            {
                string randfromkey = contents.Split('(')[1].Split(')')[0];

                string randfrom = "(UNDEFINED VARIABLE " + randfromkey + ", REPORT THIS)";
                if (script.scriptVars.ContainsKey(randfromkey))
                {
                    randfrom = script.scriptVars[randfromkey];
                }
                else
                {
                    script.scriptVars[variable] = randfrom;
                    
                    ++currentLine;
                    return true;
                }

                List<string> temparr = randfrom.Split(',').ToList();

                Random random = new Random();
                string randrep = temparr[random.Next(temparr.Count())];

                if (randrep.StartsWith(" "))
                    randrep = randrep.Substring(1);

                script.scriptVars[variable] = randrep;
            }
            else
            {
                script.scriptVars[variable] = contents;
            }
            ++currentLine;
            return true;
        }
    }

    public class GotoAction : IAction
    {
        public int LineNumber { get; set; }
        public string Destination;
        string gotoLine;

        public GotoAction(string destination, int lineNum, string igotoLine)
        {
            Destination = destination.Trim();
            LineNumber = lineNum;
            gotoLine = igotoLine;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            var lsplit = gotoLine.Split(' ');
            Destination = "";
            for (int i = 2; i < lsplit.Length; ++i)
            {
                Destination += lsplit[i];
                if (i != lsplit.Length - 1)
                    Destination += " ";
            }
            if (Destination.StartsWith("$"))
            {
                if (!script.scriptVars.TryGetValue(Destination, out Destination))
                    Destination = "END";
            }
            script.MoveToLabel(Destination.Trim());
            return true;
        }
    }


    public class MarkerAction : IAction
    {
        enum MarkerTypes
        {
            None, Add, Remove
        }
        public int LineNumber { get; set; }

        readonly MarkerTypes Command;
        public readonly string MarkerId;

        public MarkerAction(string command, string markerId, int lineNum)
        {
            LineNumber = lineNum;
            if (!Enum.TryParse<MarkerTypes>(command, true, out Command))
            {
                Command = MarkerTypes.None;
            }
            MarkerId = markerId;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            switch (Command)
            {
                case MarkerTypes.Add:
                    script.MarkerAdded(MarkerId);
                    break;
                case MarkerTypes.Remove:
                    script.MarkerRemoved(MarkerId);
                    break;
            }
            
            ++currentLine;
            return true;
        }
    }

    public class GiveItemAction : IAction
    {
        public int LineNumber { get; set; }
        public int Item;
        public int Quantity;

        public GiveItemAction(string item,string quantity , int lineNum)
        {
            int.TryParse(item, out Item);
            LineNumber = lineNum;
            int.TryParse(quantity, out Quantity);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ItemAdded(new Inventory(Item, Quantity));
            ++currentLine;
            return true;
        }
    }

    public class TakeItemAction : IAction
    {
        public int LineNumber { get; set; }
        public int Item;
        public int Quantity;

        public TakeItemAction(string item, int lineNum, string quantity = "1")
        {
            int.TryParse(item, out Item);
            LineNumber = lineNum;
            int.TryParse(quantity, out Quantity);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ItemRemoved(new Inventory(Item, Quantity));
            ++currentLine;
            return true;
        }
    }

    public class GiveXpAction : IAction
    {
        public int LineNumber { get; set; }
        public int Quantity;
        public string Type;

        public GiveXpAction(string quantity, int lineNum) : this("", quantity, lineNum)
        {
        }

        public GiveXpAction(string type, string quantity, int lineNum)
        {
            LineNumber = lineNum;
            Type = type;
            int.TryParse(quantity, out Quantity);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.GiveXp(Type, Quantity);
            ++currentLine;
            return true;
        }
    }

    public class GiveSkillAction : IAction
    {
        public int LineNumber { get; set; }
        public int Quantity;
        public int SkillId;
        public int UpgradeId;

        public GiveSkillAction(string skillId, string upgradeId, int lineNum)
        {
            LineNumber = lineNum;
            int.TryParse(skillId, out SkillId);
            int.TryParse(upgradeId, out UpgradeId);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.GiveSkill(SkillId, UpgradeId);
            ++currentLine;
            return true;
        }
    }

    public class SpawnAction : IAction
    {
        public int LineNumber { get; set; }
        public readonly string Resource;
        public readonly float X, Y, Z, Rx, Ry, Rz, Rw;

        public SpawnAction(string resource, string locationString, int lineNum)
        {
            LineNumber = lineNum;
            Resource = resource;
            var match = Regex.Match(locationString,
                @"(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+)");
            if (!match.Success) 
                throw new ArgumentException("Was expecting 8 floats, comma separated", locationString);
            float.TryParse(match.Groups[1].Value, out X);
            float.TryParse(match.Groups[2].Value, out Y);
            float.TryParse(match.Groups[3].Value, out Z);
            float.TryParse(match.Groups[4].Value, out Rx);
            float.TryParse(match.Groups[5].Value, out Ry);
            float.TryParse(match.Groups[6].Value, out Rz);
            float.TryParse(match.Groups[7].Value, out Rw);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.SpawnItem(Resource, X, Y, Z, Rx, Ry, Rz, Rw);
            ++currentLine;
            return true;
        }
    }

    public class SpawnMobAction : IAction
    {
        public int LineNumber { get; set; }
        public readonly string Resource;
        public readonly float X, Y, Z, Rx, Ry, Rz, Rw;
        public readonly int Level, MinHP, MaxHP;

        public SpawnMobAction(string resource, string locationString, string level, string minHp, string maxHp, int lineNum)
        {
            LineNumber = lineNum;
            Resource = resource.Trim();
            int.TryParse(level, out Level);
            int.TryParse(minHp, out MinHP);
            int.TryParse(maxHp, out MaxHP);

            var match = Regex.Match(locationString,
                @"(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+),\s?(-?[0-9]*\.?[0-9]+)");
            if (!match.Success)
                throw new ArgumentException("Was expecting 7 floats, comma separated", locationString);
            float.TryParse(match.Groups[1].Value, out X);
            float.TryParse(match.Groups[2].Value, out Y);
            float.TryParse(match.Groups[3].Value, out Z);
            float.TryParse(match.Groups[4].Value, out Rx);
            float.TryParse(match.Groups[5].Value, out Ry);
            float.TryParse(match.Groups[6].Value, out Rz);
            float.TryParse(match.Groups[7].Value, out Rw);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.SpawnMob(Resource, Level, MinHP, MaxHP, X, Y, Z, Rx, Ry, Rz, Rw);
            ++currentLine;
            return true;
        }
    }

    public class GrowUpAction : IAction
    {
        public int LineNumber { get; set; }
        

        public GrowUpAction(int lineNum)
        {
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.GrowUpCommand();
            ++currentLine;
            return true;
        }
    }

    public class ExecAction : IAction
    {
        public int LineNumber { get; set; }
        public string Name { get; set; }
        public string[] Params { get; set; }

        public ExecAction(string name, string parms, int lineNum)
        {
            LineNumber = lineNum;
            Name = name.Trim();
            Params = ParseArguments(parms);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ExecActionCommand(Name, Params);
            ++currentLine;
            return true;
        }

        public static string[] ParseArguments(string commandLine)
        {
            char[] parmChars = commandLine.ToCharArray();
            bool inQuote = false;
            for (int index = 0; index < parmChars.Length; index++)
            {
                if (parmChars[index] == '"')
                    inQuote = !inQuote;
                if (!inQuote && parmChars[index] == ',')
                    parmChars[index] = '\n';
            }

            var splitStrings = new string(parmChars).Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (var i = 0; i < splitStrings.Length; i++)
            {
                splitStrings[i] = splitStrings[i].Trim(new[] { ' ', '"' });
            }

            return splitStrings;
        }
    }

    public class WaitAction : IAction
    {
        public int LineNumber { get; set; }
        public readonly float WaitTime;

        public WaitAction(string waitTime, int lineNum)
        {
            LineNumber = lineNum;
            float.TryParse(waitTime, out WaitTime);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            //we tell the listeners which line they should actually resume at, otherwise they'll be stuck forever
            script.Wait(WaitTime, currentLine + 1);
            //we say to pause here
            return false;
        }
    }

    public class FlagAction : IAction
    {
        public int LineNumber { get; set; }
        public string Name;
        public bool Value;

        public FlagAction(string name, bool val, int lineNum)
        {
            Name = name;
            Value = val;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.Flags[Name] = Value;
            ++currentLine;
            return true;
        }
    }

    public class VarAction : IAction
    {
        public int LineNumber { get; set; }
        public string Name;
        public string StrVal;
        public int IntVal;

        public VarAction(string name, string val, int lineNum)
        {
            Name = name;
            StrVal = val;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (script.Variables.ContainsKey(Name))
            {
                script.Variables[Name] = this;
            }
            else
            {
                script.Variables.Add(Name, this);
            }
            ++currentLine;
            return true;
        }
    }

    public class ImpulseAction : IAction
    {

        public int LineNumber { get; set; }
        public string Context;
        public string Value;

        public ImpulseAction(string context, string val, int lineNum)
        {
            Context = context;

            Value = val;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            //todo: support variables?
            script.Impulsed(Context, Value);

            ++currentLine;
            return true;
        }
    }

    public class StartSceneAction : IAction
    {
        public int LineNumber { get; private set; }
        public readonly string SceneNameId;
        public readonly string ContextStringId;

        public StartSceneAction(string contextString, string sceneName, int lineNum)
        {
            ContextStringId = contextString;
            SceneNameId = sceneName;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.StartSceneActivated(ContextStringId, SceneNameId);
            ++currentLine;
            return true;
        }
    }

    public class ActivatePhaseAction : IAction
    {
        public int LineNumber { get; private set; }
        public readonly string Phase;
        public readonly List<int> PhaseArray = new List<int>();

        public ActivatePhaseAction(string newPhase, int lineNum)
        {
            Phase = newPhase;
            LineNumber = lineNum;

            var splitPhases = Phase.Split(',');
            foreach (var splitPhase in splitPhases)
            {
                int newParsedPhase;
                if (int.TryParse(splitPhase.Trim(), out newParsedPhase))
                {
                    PhaseArray.Add(newParsedPhase);
                }
            }
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            
            script.PhaseActivated(PhaseArray.ToArray());
            ++currentLine;
            return true;
        }
    }

    public class DeactivatePhaseAction : IAction
    {
        public int LineNumber { get; private set; }
        public readonly string Phase;
        public readonly List<int> PhaseArray = new List<int>();

        public DeactivatePhaseAction(string newPhase, int lineNum)
        {
            Phase = newPhase;
            LineNumber = lineNum;

            var splitPhases = Phase.Split(',');
            foreach (var splitPhase in splitPhases)
            {
                int newParsedPhase;
                if (int.TryParse(splitPhase.Trim(), out newParsedPhase))
                {
                    PhaseArray.Add(newParsedPhase);
                }
            }
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {

            script.PhaseDectivated(PhaseArray.ToArray());
            ++currentLine;
            return true;
        }
    }

    public class ActivateObjectiveAction : IAction
    {
        public int LineNumber { get; private set; }
        public readonly string ObjectiveID;
        public readonly string QuestId;

        public ActivateObjectiveAction(string quest, string objective, int lineNum)
        {
            QuestId = quest;
            ObjectiveID = objective;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ObjectiveActivated(QuestId, ObjectiveID);
            ++currentLine;
            return true;
        }
    }

    public class CompleteObjectiveAction : IAction
    {
        public int LineNumber { get; private set; }
        public readonly string ObjectiveID;
        public readonly string QuestId;

        public CompleteObjectiveAction(string quest, string objectiveId, int lineNum)
        {
            QuestId = quest;
            ObjectiveID = objectiveId;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ObjectiveCompleted(QuestId, ObjectiveID);
            ++currentLine;
            return true;
        }
    }

    public class CancelObjectiveAction : IAction
    {
        public int LineNumber { get; private set; }
        public readonly string ObjectiveID;
        public readonly string QuestId;

        public CancelObjectiveAction(string quest, string objectiveId, int lineNum)
        {
            QuestId = quest;
            ObjectiveID = objectiveId;
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ObjectiveCanceled(QuestId, ObjectiveID);
            ++currentLine;
            return true;
        }
    }

    public class ActivateQuestAction : IAction
    {
        public int LineNumber { get; set; }
        public string Quest;

        private const int MINIMUM_QUEST_STAGE = 0;

        public ActivateQuestAction(string quest, int lineNum)
        {
            Quest = quest;
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.QuestActivated(Quest);
            ++currentLine;
            return true;
        }
    }

    public class QuestStageAction : IAction
    {
        public int LineNumber { get; set; }
        public string Quest;
        public int Stage;
        Func<int, int> op;
        private const int MINIMUM_QUEST_STAGE = 0;

        public QuestStageAction(string quest, string stage, int lineNum)
        {
            Quest = quest;
            stage = stage.Trim();
            if (stage.Length > 0)
                switch(stage[0])
                {
                    case '+': op = s => s + Stage; break;
                    case '-': op = s => s - Stage; break;
                    case '*': op = s => s * Stage; break;
                    case '/': op = s => s / Stage; break;
                    default: op = null; break;
                }

            //get rid of the operator if we detected one.
            if (op != null)
                stage = stage.Substring(1);

            int.TryParse(stage, out Stage);
            LineNumber = lineNum;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            int stage;
            if (op != null)
            {
                if (!script.PlayerQuestStage(Quest, out stage))
                    stage = 0;

                stage = op(stage);
            }
            else
                stage = Stage;

            if (stage < MINIMUM_QUEST_STAGE)
                script.QuestStageRemoved(Quest);
            else
                script.QuestStageChanged(Quest, stage);

            ++currentLine;
            return true;
        }
    }

    public class PlaySoundAction : IAction
    {
        public int LineNumber { get; set; }
        public string SoundResourceFile;

        public PlaySoundAction(string soundResourceFile, int lineNum)
        {
            SoundResourceFile = soundResourceFile;
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.PlaySound(SoundResourceFile);
            ++currentLine;
            return true;
        }
    }

    public class CompleteQuestAction : IAction
    {
        public int LineNumber { get; set; }
        public string Quest;

        public CompleteQuestAction(string quest, int lineNum)
        {
            Quest = quest;
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.QuestCompleted(Quest);
            ++currentLine;
            return true;
        }
    }

    public class JournalAction : IAction
    {
        public string Quest;
        public string JournalEntry;

        public JournalAction(string questName, string journalEntry, int lineNumber)
        {
            LineNumber = lineNumber;
            Quest = questName.Trim();
            JournalEntry = journalEntry.Trim();
        }

        public int LineNumber { get; set; }
        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.JournalEntryEvent(Quest, JournalEntry);

            currentLine++;
            return true;
        }
    }

    public class ErrorAction : IAction
    {
        public int LineNumber { get; set; }
        public ErrorType Type;
        public List<string> Parameters;
        public string Line;

        public ErrorAction(ErrorType type, List<string> parameters, int lineNum, string line)
        {
            Type = type;
            Parameters = parameters;
            LineNumber = lineNum;
            Line = line;
        }
			
        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            script.ScriptErrored("Something is wrong with the script " + script.Script.Name + ": " + Type + ". Line " + LineNumber + " params: " + Parameters);

            currentLine = int.MaxValue;
            return false;
        }
    }

    #region not yet implemented
    public class FaceAction : IAction
    {
        public int LineNumber { get; set; }
        public string Name;
        public string Direction;

        public FaceAction(string name, string direction, int lineNum)
        {
            Name = name;
            Direction = direction;
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            throw new NotImplementedException();
        }
    }

    public class EmoteAction : IAction
    {
        public int LineNumber { get; set; }
        public string Name;
        public Emotions Emotion;

        public EmoteAction(string name, string emotion, int lineNum)
        {
            Name = name;
            if (!Enum.TryParse<Emotions>(emotion, true, out Emotion))
                Emotion = Emotions.none;
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            throw new NotImplementedException();
        }
    }

    

    public class RemoveAction : IAction
    {
        public int LineNumber { get; set; }
        public string Target;

        public RemoveAction(string target, int lineNum)
        {
            Target = target;
            LineNumber = lineNum;
        }


        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            throw new NotImplementedException();
        }
    }
#endregion

    #region conditionals

    public class MultiConditional : IAction, IConditional
    {
        private readonly IConditional _left;
        private readonly IConditional _right;
        private readonly Comparator _compare;

        public enum Comparator
        {
            And,
            Or,
            Xor,
        }

        public int LineNumber { get; set; }
        public string Destination { get; set; }

        public MultiConditional(IConditional left, IConditional right, Comparator compare, string dest, int lineNum)
        {
            _left = left;
            _right = right;
            _compare = compare;
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            switch (_compare)
            {
                case Comparator.And:
                    return _left.Query(script) && _right.Query(script);
                case Comparator.Or:
                    return _left.Query(script) || _right.Query(script);
                case Comparator.Xor:
                    return _left.Query(script) ^ _right.Query(script);
                default:
                    return false;
            }
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class FlagSetAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public string Name;
        public string Destination { get; set; }

        public FlagSetAction(string name, string dest, int lineNum)
        {
            Name = name;
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.Flags.ContainsKey(Name) && script.Flags[Name];
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
            {
                script.MoveToLabel(Destination);
            }
            else
                ++currentLine;

            return true;
        }
    }

    public class HasItemAnywhereAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public List<int> Item, Quantity;
        public string Destination { get; set; }
        
        public HasItemAnywhereAction(string itemVal, string dest, int lineNum)
        {
            //detect csv items
            var allItems = itemVal.Split(',');
            Item = new List<int>(allItems.Length);
            Quantity = new List<int>(allItems.Length);

            foreach (var item in allItems)
            {
                int itemId, quant = 1;
                if (int.TryParse(item, out itemId) || (item.Contains(':') && int.TryParse(item.Split(':')[0], out itemId) && int.TryParse(item.Split(':')[1], out quant)))
                {
                    Item.Add(itemId);
                    Quantity.Add(quant);
                }
            }

            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            bool enoughItems = true;
            for (int i = 0; i < Item.Count; i++)
            {
                if (!script.PlayerHasItemAnywhere(new Inventory(Item[i], Quantity[i])))
                {
                    enoughItems = false;
                    break;
                }
            }
            return enoughItems;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class HasItemAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public List<int> Item, Quantity;
        public string Destination { get; set; }
        
        public HasItemAction(string itemVal, string dest, int lineNum)
        {
            //detect csv items
            var allItems = itemVal.Split(',');
            Item = new List<int>(allItems.Length);
            Quantity = new List<int>(allItems.Length);

            foreach (var item in allItems)
            {
                int itemId, quant = 1;
                if (int.TryParse(item, out itemId) || (item.Contains(':') && int.TryParse(item.Split(':')[0], out itemId) && int.TryParse(item.Split(':')[1], out quant)))
                {
                    Item.Add(itemId);
                    Quantity.Add(quant);
                }
            }

            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            bool enoughItems = true;
            for (int i = 0; i < Item.Count; i++)
            {
                if (!script.PlayerHasItem(new Inventory(Item[i], Quantity[i])))
                {
                    enoughItems = false;
                    break;
                }
            }
            return enoughItems;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class HasItemEquippedAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public List<int> Item;
        public string Destination { get; set; }

        public HasItemEquippedAction(string itemVal, string dest, int lineNum)
        {
            //detect csv items
            var allItems = itemVal.Split(',');
            Item = new List<int>(allItems.Length);

            foreach (var item in allItems)
            {
                int itemId, quant = 1;
                if (int.TryParse(item, out itemId))
                {
                    Item.Add(itemId);
                }
            }

            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            bool enoughItems = true;
            for (int i = 0; i < Item.Count; i++)
            {
                if (!script.PlayerHasItemEquipped(new Inventory(Item[i], 1)))
                {
                    enoughItems = false;
                    break;
                }
            }
            return enoughItems;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {

            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class HasCutieMarkAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public int CutieMark;
        public string Destination { get; set; }

        public HasCutieMarkAction(string cutiemark, string dest, int lineNum)
        {
            int.TryParse(cutiemark, out CutieMark);
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.PlayerHasCutieMark(CutieMark);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class IsRaceAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public Race race;
        public string Destination { get; set; }

        public IsRaceAction(string race, string dest, int lineNum)
        {
            if (!Enum.TryParse<Race>(race, true, out this.race))
                this.race = Race.None;

            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.PlayerRace() == race;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class ObjectiveActiveCondition : AConditional
    {
        public string QuestId { get; private set; }
        public string ObjectiveId { get; private set; }

        public ObjectiveActiveCondition(string quest, string objective, string dest, int lineNum)
            : base(dest, lineNum)
        {
            QuestId = quest;
            ObjectiveId = objective;
        }

        public override bool Query(ScriptRunner script)
        {
            return script.PlayerObjectiveIsActive(QuestId, ObjectiveId);
        }
    }

    public class ObjectiveCompleteCondition : IAction, IConditional
    {
        public int LineNumber { get; private set; }
        public readonly string ObjectiveId;
        public readonly string QuestId;

        public string Destination { get; set; }
        
        public ObjectiveCompleteCondition(string quest, string objective, string dest, int lineNum)
        {
            QuestId = quest;
            ObjectiveId = objective;
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.PlayerObjectiveIsComplete(QuestId, ObjectiveId);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class QuestActiveAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public string Quest;
        public string Destination { get; set; }

        public QuestActiveAction(string quest, string dest, int lineNum)
        {
            Quest = quest;
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.PlayerQuestIsActive(Quest);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }
    public class QuestStagedAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public string Quest;
        public int CompareTo;
        public int Stage;
        public string Destination { get; set; }

        public QuestStagedAction(string quest, string comparator, string stage, string dest, int num)
        {
            // TODO: Complete member initialization
            Quest = quest;
            switch (comparator)
            {
                case "=":
                    CompareTo = 0;
                    break;
                case ">":
                    CompareTo = 1;
                    break;
                case "<":
                    CompareTo = -1;
                    break;
                default:
                    CompareTo = 0;
                    break;
            }
            
            int.TryParse(stage, out Stage);
            Destination = dest;
            LineNumber = num;
        }

        public bool Query(ScriptRunner script)
        {
            int stage;
            if (!script.PlayerQuestStage(Quest, out stage))
                stage = 0;
            return stage.CompareTo(Stage) == CompareTo;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
            {
                script.MoveToLabel(Destination);
            }
            else
            {
                ++currentLine;
            }

            return true;
        }
    }

    public class QuestCompleteAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public string Quest;
        public string Destination { get; set; }

        public QuestCompleteAction(string quest, string dest, int lineNum)
        {
            Quest = quest;
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.PlayerCompletedQuest(Quest);
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class AgeCheckAction : IAction, IConditional
    {
        public int LineNumber { get; set; }
        public Gender genderAndAge;
        public string Destination { get; set; }

        public AgeCheckAction(string gender, string dest, int lineNum)
        {
            if (!Enum.TryParse<Gender>(gender, true, out this.genderAndAge))
                this.genderAndAge = Gender.Mare;
            Destination = dest;
            LineNumber = lineNum;
        }

        public bool Query(ScriptRunner script)
        {
            return script.PlayerGender() == genderAndAge;
        }

        public bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }

    public class QueryExecAction : AConditional
    {
        public bool Exclamation { get; set; }
        public string Name { get; set; }
        public string[] Params { get; set; }

        public QueryExecAction(string exclamation, string name, string parms, string dest, int lineNum)
            : base(dest, lineNum)
        {
            Exclamation = !string.IsNullOrEmpty(exclamation);
            Name = name.Trim();
            Params = ExecAction.ParseArguments(parms);
        }

        public override bool Query(ScriptRunner script)
        {
            return script.GetQueryExec(Exclamation, Name, Params);
        }
    }
    #endregion
}
