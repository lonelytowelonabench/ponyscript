﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PonyScript
{
    public interface IAction
    {
        int LineNumber { get; }

        /// <summary>
        /// invoke the action
        /// </summary>
        /// <param name="script"></param>
        /// <param name="currentLine"></param>
        /// <returns>true if the scriptrunner should run again</returns>
        bool Invoke(ScriptRunner script, ref int currentLine);
    }
}
