﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PonyScript
{
    public interface IConditional
    {
        string Destination { get; set; }
        bool Query(ScriptRunner script);
    }

    public abstract class AConditional : IAction, IConditional
    {
        public int LineNumber { get; private set; }
        public string Destination { get; set; }

        protected AConditional(string destination, int lineNumber)
        {
            Destination = destination;
            LineNumber = lineNumber;
        }

        public abstract bool Query(ScriptRunner script);
        
        public virtual bool Invoke(ScriptRunner script, ref int currentLine)
        {
            if (Query(script))
                script.MoveToLabel(Destination);
            else
                ++currentLine;

            return true;
        }
    }
}
