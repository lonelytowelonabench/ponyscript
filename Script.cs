﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;

namespace PonyScript {
	public class Script {

		public string Name;
		public List<IAction> Lines;
		public Dictionary<string, int> Labels;

		public string Location;
		
		private static Dictionary<string, Script> CachedScripts = new Dictionary<string, Script>();

		public static List<string> Statements = new List<string>() {
			"goto",
			"face",
			"emote",
			"giveitem",
			"takeitem",
            "givexp",
            "flag",
			"setflag",
			"remove",
			"itemset?",
			"hasitemanywhere?",
			"hasitem?",
            "hasitemequipped?",
            "hascutiemark?",
            "israce?",
            "questactive?",
            "activatequest",
            "questcomplete?",
            "objectivecomplete?",
            "activateobjective",
            "cancelobjective",
            "completeobjective",
            "isage?",
            "marker",
            "impulse",
            "spawnitem",
            "spawnMob",
            "wait",
		};

		public Script() {
			Lines = new List<IAction>();
			Labels = new Dictionary<string, int>();
		}

        public bool HasErrors()
        {
            return HasErrors(out var ignore);
        }

        public bool HasErrors(out ErrorAction[] errorLine)
        {
            var errors = new List<ErrorAction>();
            foreach (var action in Lines)
            {
                if(action is ErrorAction)
                    errors.Add((ErrorAction)action);
            }
            errorLine = errors.ToArray();
            return errorLine.Any();
        }

        #region Loading/Parsing
        private static string StripComments(string line)
        {
            return Regex.Replace(line, "(#.+)", "");
        }

        private delegate IAction RecursiveLineParse(
	        Match match, string[] args, string line, int lineNumber, string[] recursiveArgs);

        private struct LineParser
        {
            private Regex regex;
            private Func<Match, string[], string, int, IAction> makeAction;
            private RecursiveLineParse makeActionRecurse;

#if DEBUG
            // debugging+profiling fields:
            public Stopwatch sw;
            public int succeeded;
#endif

            public LineParser(Regex r, Func<Match, string[], string, int, IAction> a)
            {
                regex = r;
                makeAction = a;
                makeActionRecurse = null;
#if DEBUG
                sw = new Stopwatch();
                succeeded = 0;
#endif
            }

            public LineParser(Regex r, RecursiveLineParse a)
            {
                regex = r;
                makeAction = null;
                makeActionRecurse = a;
#if DEBUG
                sw = new Stopwatch();
                succeeded = 0;
#endif
            }

            public bool Parse(string line, int num, out IAction action, string[] recursivePass)
            {
                action = null;

#if DEBUG
                sw.Start();
#endif
                var match = regex.Match(line);

                if (match.Success)
                {
                    var args = regex.Split(line);

                    if (makeAction != null)
                        try
                        {
                            action = makeAction(match, args, line, num);
                        }
                        catch (Exception e)
                        {
                            action = new ErrorAction(ErrorType.InvalidArguments, new List<string>(args){e.ToString()}, num, line);
                        }
                    else
                        action = makeActionRecurse(match, args, line, num, recursivePass);
#if DEBUG
                    succeeded++;
#endif
                }
#if DEBUG
                sw.Stop();
#endif
                return match.Success;
            }

#if DEBUG
            public override string ToString()
            {
                return "(" + succeeded + ") " + sw.Elapsed.TotalSeconds.ToString() + "s  (average " + sw.Elapsed.TotalSeconds/succeeded + "s)";
            }
#endif
        }

#if DEBUG
        public static string ParseElapsed
        {
            get
            {
                return 
                    (
                    asteriskLineParsers.Sum(a => a.sw.Elapsed.TotalSeconds)
                    + arrowBracketLineParsers.Sum(a => a.sw.Elapsed.TotalSeconds)
                    + curlyBracketLineParsers.Sum(a => a.sw.Elapsed.TotalSeconds)
                    + squareBracketLineParsers.Sum(a => a.sw.Elapsed.TotalSeconds)
                    + otherLineParsers.Sum(a => a.sw.Elapsed.TotalSeconds)
                    ) + " total seconds";
            }
        }
#endif

        public Dictionary<string, string> Aliases { get; private set; }

        private static LineParser[] asteriskLineParsers = new LineParser[]
        {
            new LineParser(new Regex(@"\*(?:\s*)(?i)aliasname(?:\s*)<([^>]+?)>(?:\s*)<([^>]+?)>"),
                (m, args, line, num) => new AliasAction(m.Groups[1].Value, m.Groups[2].Value)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)marker(?:\s+)(add|remove)(?:\s+)(.+)"),
                (m, args, line, num) => new MarkerAction(m.Groups[1].Value, m.Groups[2].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)goto(?:\s+)(.+)"),
                (m, args, line, num) => new GotoAction(args[1], num, line)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)face(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new FaceAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s+)(?i)emote(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new EmoteAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)runscript(?:\s+)(.+)\sgoto\s\[(.+)]"), // run script and jump
                (m, args, line, num) => new RunScriptAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)runscript(?:\s+)(.+)"),
                (m, args, line, num) => new RunScriptAction(args[1], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)giveitem\s(-?[0-9\.]+)\s([0-9\.]+)(\s+)?"),
                (m, args, line, num) => new GiveItemAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)takeitem\s(-?[0-9\.]+)\s([0-9\.]+)(\s+)?"),
                (m, args, line, num) => new TakeItemAction(args[1], num, args[2])),
            new LineParser(new Regex(@"\*\s*(?i)givexp\s+<(.+)>\s+(-?[0-9\.]+)?"),
                (m, args, line, num) => new GiveXpAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)givexp\s(-?[0-9\.]+)?"),
                (m, args, line, num) => new GiveXpAction(args[1], num)),
            new LineParser(new Regex(@"\*\s*(?i)spawn\s*<\s*(.+)\s*>\s*(.+)"),
                (m, args, line, num) => new SpawnAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*\s*(?i)spawnmob\s*(.+)\s*<\s*(.+)\s*>\s+(.+)\s+(.+)\s+(.+)"),
                (m, args, line, num) => new SpawnMobAction(args[1], args[2], args[3], args[4], args[5], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)growUpCommand\s(-?[0-9\.]+)?"),
                (m, args, line, num) => new GrowUpAction(num)),
            new LineParser(new Regex(@"\*(?:\s*)f(?:\s+)(.+)(?:\s*)\((.*)\)"), 
                (m, args, line, num) => new ExecAction(args[1], args[2], num)), 
            new LineParser(new Regex(@"\*\s*wait\s+(.+)"),
                (m, args, line, num) => new WaitAction(args[1], num)), 
            new LineParser(new Regex(@"\*(?:\s*)(?i)activateobjective(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new ActivateObjectiveAction(m.Groups[1].Value, m.Groups[2].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)completeobjective(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new CompleteObjectiveAction(m.Groups[1].Value, m.Groups[2].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)cancelobjective(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new CancelObjectiveAction(m.Groups[1].Value, m.Groups[2].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)activatequest(?:\s+)<(.+?)>"),
                (m, args, line, num) => new ActivateQuestAction(args[1], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)queststage(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new QuestStageAction(args[1], args[2] ,num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)completequest(?:\s+)(.+)"),
                (m, args, line, num) => new CompleteQuestAction(args[1], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)playsound(?:\s+)(.+)"),
                (m, args, line, num) => new PlaySoundAction(args[1], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)activatephase(?:\s+)(.+)"),
                (m, args, line, num) => new ActivatePhaseAction(m.Groups[1].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)deactivatephase(?:\s+)(.+)"),
                (m, args, line, num) => new DeactivatePhaseAction(m.Groups[1].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)giveSkill\s(-?[0-9\.]+)\s(-?[0-9\.]+)?"),
                (m, args, line, num) => new GiveSkillAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)flag(?:\s+)(.+)"),
                (m, args, line, num) => new FlagAction(args[1], true, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)setflag(?:\s+)(.+)(?:\s+)(.+)"),
                (m, args, line, num) => new FlagAction(args[1], (args[2] == "true" || args[2] == "on" || args[2] == "1") ? true : false, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)startScene(?:\s+)<(.+)>(?:\s+)(.+)"),
                (m, args, line, num) => new StartSceneAction(m.Groups[1].Value, m.Groups[2].Value, num)),
            new LineParser(new Regex(@"\* \$(.+)=(.+)"), // Variable - * $i = yes
                (m, args, line, num) =>
                {
                    var variable = line.Split('=')[0];
                    variable = variable.Substring(2);
                    if (variable.EndsWith(" "))
                        variable = variable.Substring(0, variable.Length - 1);

                    var val = line.Split('=')[1];
                    if (val.StartsWith(" "))
                        val = val.Substring(1);

                    return new VarSetAction(variable, val);   
                }),
            new LineParser(new Regex(@"\*(?:\s*)impulse(?:\s+)<(.+)>(?:\s+)<(.+)>"),
                (m, args, line, num) => new ImpulseAction(m.Groups[1].Value, m.Groups[2].Value, num)),
            new LineParser(new Regex(@"\* *if (.+)=(.+):(.+)"),
                (m, args, line, num) => new VarConditionalAction(line)),
            new LineParser(new Regex(@"\* *shop (.+)"),
                (m, args, line, num) => new ShopAction(line)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)remove(?:\s+)(.+)"),
                (m, args, line, num) => new RemoveAction(args[1], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)remove"),
                (m, args, line, num) => new RemoveAction("", num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)flagset\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new FlagSetAction(args[1], args[2], num)),
            
            //conditionals for posterity
            new LineParser(new Regex(@"\*(?:\s*)(?i)hasitemanywhere\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new HasItemAnywhereAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)hasitem\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new HasItemAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)hasitemequipped\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new HasItemEquippedAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)hascutiemark\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new HasCutieMarkAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)israce\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new IsRaceAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)objectiveactive\?(?:\s+)<(.+)>(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new ObjectiveActiveCondition(m.Groups[1].Value, m.Groups[2].Value, m.Groups[3].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)objectivecomplete\?(?:\s+)<(.+)>(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new ObjectiveCompleteCondition(m.Groups[1].Value, m.Groups[2].Value, m.Groups[3].Value, num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)questactive\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new QuestActiveAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)queststage\?(?:\s+)<(.+)>\s([=\><])\s(-?[0-9]+)\s:(.+)"),
                (m, args, line, num) => { var split = args; return new QuestStagedAction(split[1], split[2], split[3], split[4], num); }),
            new LineParser(new Regex(@"\*(?:\s*)(?i)questcomplete\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new QuestCompleteAction(args[1], args[2], num)),
            new LineParser(new Regex(@"\*(?:\s*)(?i)isage\?(?:\s+)<(.+)>(?:\s+):(.+)"),
                (m, args, line, num) => new AgeCheckAction(args[1], args[2], num)),

            new LineParser(new Regex(@"\*(?:\s*)([a-zA-Z]+)"),
                (m, args, line, num) => new ErrorAction(Statements.Contains((args[1])) ? ErrorType.InvalidArguments : ErrorType.InvalidStatement, new List<string>() { (args[1]) }, num, line)),
        };

        static LineParser[] queryLineParsers =
        {
            new LineParser(new Regex(@"\?\s*(.+)\s+:(.+)"),
                (m, args, line, num) => ParseLine(m.Groups[1].Value, num, conditionalParsers, m.Groups[2].Value.Trim())),
        };

        static LineParser[] conditionalParsers =
        {
            // this has to be first, otherwise it gets messed up by the order of operations parser
            new LineParser(new Regex(@"f\?(?:\s+)(?<Exclamation>!)?(\w+?)(?:\s*)\((.*?)\)"),
                (m, args, line, num, recursargs) => new QueryExecAction(
                    m.Groups["Exclamation"].Value.Trim(), // Exclamation
                    m.Groups[1].Value.Trim(),             // Name
                    m.Groups[2].Value.Trim(),             // Parameters
                    recursargs[0], num)),


            //order of operations.
            //todo: isn't supported properly, as this will parse out -> in, instead of left -> right
            // given something like (a | b) | (b | a)
            // will be parsed as (  a|b)|(b|a  )
            // (a | (a | b)) would be fine though.
            new LineParser(new Regex(@"\((.+)\)"),
                (m, args, line, num, recurseArgs) => ParseLine(m.Groups[1].Value, num, conditionalParsers, recurseArgs)),
            
            //todo: same goes for these. parses out -> in instead of left -> right
            // a | a & b = a|a  &  b
            // but a & a | b = a  &  a|b
            new LineParser(new Regex(@"(.+)\s*\|\s*(.+)"),
                (m, args, line, num, recurseArgs) =>
                {
                    //recursively get all the conditionals...
                    var left = ParseLine(m.Groups[1].Value, num, conditionalParsers, recurseArgs) as IConditional;
                    var right = ParseLine(m.Groups[2].Value, num, conditionalParsers, recurseArgs) as IConditional;
                    if (left == null || right == null)
                        return new ErrorAction(ErrorType.InvalidStatement, new List<string> { m.Groups[1].Value, m.Groups[2].Value }, num, line);
                    return new MultiConditional(left, right, MultiConditional.Comparator.Or, recurseArgs[0], num);
                }),
            new LineParser(new Regex(@"(.+)\s*\&\s*(.+)"),
                (m, args, line, num, recurseArgs) =>
                {
                    //recursively get all the conditionals...
                    var left = ParseLine(m.Groups[1].Value, num, conditionalParsers, recurseArgs) as IConditional;
                    var right = ParseLine(m.Groups[2].Value, num, conditionalParsers, recurseArgs) as IConditional;
                    if (left == null || right == null)
                        return new ErrorAction(ErrorType.InvalidStatement, new List<string> { m.Groups[1].Value, m.Groups[2].Value }, num, line);
                    return new MultiConditional(left, right, MultiConditional.Comparator.And, recurseArgs[0], num);
                }),
                new LineParser(new Regex(@"(.+)\s*\^\s*(.+)"),
                (m, args, line, num, recurseArgs) =>
                {
                    //recursively get all the conditionals...
                    var left = ParseLine(m.Groups[1].Value, num, conditionalParsers, recurseArgs) as IConditional;
                    var right = ParseLine(m.Groups[2].Value, num, conditionalParsers, recurseArgs) as IConditional;
                    if (left == null || right == null)
                        return new ErrorAction(ErrorType.InvalidStatement, new List<string> { m.Groups[1].Value, m.Groups[2].Value }, num, line);
                    return new MultiConditional(left, right, MultiConditional.Comparator.Xor, recurseArgs[0], num);
                }),
            
            //then the conditionals
            new LineParser(new Regex(@"hasitemanywhere\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new HasItemAnywhereAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"hasitem\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new HasItemAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"hasitemequipped\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new HasItemEquippedAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"hascutiemark\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new HasCutieMarkAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"israce\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new IsRaceAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"objectiveactive\?\s+<(.+)>\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new ObjectiveActiveCondition(m.Groups[1].Value.Trim(), m.Groups[2].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"objectivecomplete\?\s+<(.+)>\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new ObjectiveCompleteCondition(m.Groups[1].Value.Trim(), m.Groups[2].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"questactive\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new QuestActiveAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"queststage\?\s+<(.+)>\s+([=\><])\s+(-?[0-9]+)"),
                (m, args, line, num, recurseArgs) => new QuestStagedAction(m.Groups[1].Value.Trim(), m.Groups[2].Value.Trim(), m.Groups[3].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"questcomplete\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new QuestCompleteAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
            new LineParser(new Regex(@"isage\?\s+<(.+)>"),
                (m, args, line, num, recurseArgs) => new AgeCheckAction(m.Groups[1].Value.Trim(), recurseArgs[0], num)),
        };

        private static LineParser[] arrowBracketLineParsers = new LineParser[]
        {
            new LineParser(new Regex(@"> *\|(.+)\| *(.+)\s+:(.+)"),
                (m, args, line, num) =>
                {
                    var condition = ParseLine(m.Groups[1].Value, num, conditionalParsers, "") as IConditional;
                    if (condition == null)
                    {
                        return new ErrorAction(ErrorType.InvalidArguments, new List<string> {m.Groups[1].Value}, num, line);
                    }
                    return new ChoiceAction(m.Groups[2].Value, condition, m.Groups[3].Value, true, num);
                }),
            new LineParser(new Regex(@"> *\|(.+)\| *(.+)\s+(.+)"),
                (m, args, line, num) =>
                {
                    var condition = ParseLine(m.Groups[1].Value, num, conditionalParsers, "") as IConditional;
                    if (condition == null)
                    {
                        return new ErrorAction(ErrorType.InvalidArguments, new List<string> {m.Groups[1].Value}, num, line);
                    }
                    return new ChoiceAction(m.Groups[2].Value, condition, m.Groups[3].Value, true, num);
                }),
            new LineParser(new Regex(@"> *\|(.+)\| *(.+)"), 
                (m, args, line, num) =>
                {
                    var condition = ParseLine(m.Groups[1].Value, num, conditionalParsers, "") as IConditional;
                    if (condition == null)
                    {
                        return new ErrorAction(ErrorType.InvalidArguments, new List<string> { m.Groups[1].Value }, num, line);
                    }
                    return new ChoiceAction(m.Groups[2].Value, condition, "NEXT", true, num);
                }),  
            new LineParser(new Regex(@"> *(\[.+\])(?:\s+:)(.+)"),
                (m, args, line, num) => new ChoiceAction(args[1], args[2], false, num)),
            new LineParser(new Regex(@"> *(.+)(?:\s+:)(.+)"),
                (m, args, line, num) => new ChoiceAction(args[1], args[2], true, num)),
            new LineParser(new Regex(@"> *(.+)"),
                (m, args, line, num) => new ChoiceAction(args[1], "NEXT", true, num)),
        };

        private static LineParser[] curlyBracketLineParsers = new LineParser[]
        {
            new LineParser(new Regex(@"{\[(.+)](.+)}"),
                (m, args, line, num) => { var rsplits = args; return new JournalAction(rsplits[1], rsplits[2], num); }),
        };

        private static LineParser[] squareBracketLineParsers = new LineParser[]
        {
            new LineParser(new Regex(@"\[(.+)\]"),
                (m, args, line, num) => new LabelAction(args[1], num)),
        };

        private static LineParser[] otherLineParsers = new LineParser[]
        {
            new LineParser(new Regex(@"(?:(?:([^:]+)(?:\((.+?)\))|(.+?)):(.+)|(.+))"), // this should parse every possible DialogueAction FAST
                (m, args, line, num) =>
            {
                switch(args.Length)
                {
                    case 5:
                        return new DialogueAction(args[1].Trim(), args[3].Trim(), args[2].Trim(), num);
                    case 4:
                        return new DialogueAction(args[1].Trim(), args[2].Trim(), "", num);
                    default:
                        return new DialogueAction(null, args[1], "", num);
                }
            }),
            /*new LineParser(new Regex(@"(.+)(?:\s*)\((.+)\)(?:\s*)\(([A-Za-z0-9+/]{1,5})\):(?:\s+)(.+)"),
                (m, args, line, num) => new DialogueAction(args[1], args[4], args[2], args[3], num)),
            new LineParser(new Regex(@"(.+)(?:\s*)\((.+)\) *:(?:\s+)(.+)"), // Emotive dialogue
                (m, args, line, num) => new DialogueAction(args[1], args[3], args[2], num)),
            new LineParser(new Regex(@"(.+):(?:\s*)(.+)"),
                (m, args, line, num) => new DialogueAction(args[1], args[2], "", num)),
            new LineParser(new Regex(@":(?:\s*)(.+)"),
                (m, args, line, num) => new DialogueAction("NARRATOR", (args[1]), "", num)),
            new LineParser(new Regex(@"\(([A-Za-z0-9+/]{1,5})\):(?:\s*)(.+)"),
                (m, args, line, num) => new DialogueAction(null, args[2], "", args[1], num)),
            new LineParser(new Regex(@"(?:\s*)(.+)"),
                (m, args, line, num) => new DialogueAction(null, args[1], "", num)),*/
        };

	    private static string[] _originalLines;

	    private static IAction ParseLine(string line, int num, LineParser[] lineParsers = null, params string[] recursivePass)
        {
            IAction action;

	        if (lineParsers == null)
	        {
	            switch (line[0])
	            {
	                case '*':
	                    lineParsers = asteriskLineParsers;
	                    break;
	                case '>':
	                    lineParsers = arrowBracketLineParsers;
	                    break;
	                case '{':
	                    lineParsers = curlyBracketLineParsers;
	                    break;
	                case '[':
	                    lineParsers = squareBracketLineParsers;
	                    break;
                    case '?':
	                    lineParsers = queryLineParsers;
	                    break;
	                default:
	                    lineParsers = otherLineParsers;
	                    break;
	            }
	        }

	        for(int i = 0; i < lineParsers.Length; i++)
            {
                if(lineParsers[i].Parse(line, num, out action, recursivePass))
                {
                    return action;
                }
            }

            // Error handling?
            return new ErrorAction(ErrorType.InvalidStatement, new List<string>() { line }, num, line);
        }

        public static Script LoadFromFile(string file, bool useCache = true)
        {
            if (file == null || file == "" || !File.Exists(file))
            {
                return null;
            }
						
						if(CachedScripts.ContainsKey(file) && useCache)
                return CachedScripts[file];

            System.IO.StreamReader f = new System.IO.StreamReader(file);
            var str = f.ReadToEnd();
            f.Close();
            var script = Load(str, file, useCache);
            CachedScripts[file] = script;
            return script;
        }

        public static event Action<Script> ScriptLoaded;

        public static Script Load(string lines, string name = "", bool useCache = true)
        {
            Script s = new Script();
            int lineNum = 0;

            const string BeginLabel = "BEGINNING";
            const string EndLable = "END";

            s.Lines.Add(new LabelAction(BeginLabel, 0));

            s.Aliases = new Dictionary<string, string>();

            _originalLines = lines.Split('\n');

            foreach (var line in _originalLines)
            {
                lineNum++;
                string modline = StripComments(line);
                modline = modline.Trim();

                if (modline == "") continue;
                
                var a = ParseLine(modline, lineNum);

                if (a is LabelAction)
                {
                    var la = a as LabelAction;
                    if (la.Name == BeginLabel)
                    {
                        //the writer has determined to ignore everything before this line
                        //label will be added at end of loop
                        s.Lines.Clear();
                    }
                    else if (la.Name == EndLable)
                    {
                        //the writer has determined to ignore everything after this line
                        //end lable will be added by exiting.
                        break;
                    }
                }

                //specific preprocessing
                if (a is DialogueAction)
                {
                    var da = a as DialogueAction;

                    //if (da.Character != null && s.Aliases.ContainsKey(da.Character))
                    //{
                    //    da.Character = s.Aliases[da.Character];
                    //}
                }

                if (a is RunScriptAction)
                {
                    var ra = a as RunScriptAction;
                    var parDir = Directory.GetParent(name);
                    if (parDir.Exists)
                    {
                        var nScript = Path.Combine(parDir.FullName, ra.ScriptPath);
                        if (File.Exists(nScript))
                        {
                            //load the extra script
                            ra.LoadScript(nScript, useCache);
                        }
                    }
                }

                //whether or not to add to the lines
                var aliasAction = a as AliasAction;
                if (aliasAction != null)
                {
                    if (s.Aliases.ContainsKey(aliasAction.CharacterToAlias))
                        s.Aliases[aliasAction.CharacterToAlias] = aliasAction.NewAlias;
                    else
                        s.Aliases.Add(aliasAction.CharacterToAlias, aliasAction.NewAlias);
                }
                else if (a != null)
                {
                    s.Lines.Add(a);
                }
            }

            s.Lines.Add(new LabelAction(EndLable, 0));

            // Make a list of labels
            for (int i = 0; i < s.Lines.Count; ++i)
            {
                if (s.Lines[i] is LabelAction)
                {
                    var action = (LabelAction)s.Lines[i];
                    if (s.Labels.ContainsKey(action.Name))
                    {
                        throw new Exception(String.Format("Error on line {0} in {2}: Could not add label {1} as it is already in use", action.LineNumber, action.Name, name));
                    }
                    s.Labels.Add(action.Name, i);
                }
            }

            //change all the 'null' character lines to be for their respective character
            var lastEmotion = Emotions.neutral;
            foreach (IAction a in s.Lines)
            {
                var diag = a as DialogueAction;
                if (diag == null) continue;
                
                if (diag.Character == null)
                {
                    diag.Emotion = lastEmotion;
                }
                else
                {
                    lastEmotion = diag.Emotion;
                }
            }
            s.Name = name;

            if (ScriptLoaded != null)
            {
                try
                {
                    ScriptLoaded(s);
                }
                catch (Exception) { }
            }
            return s;
        }
        #endregion

        internal string GetOriginalLine(int lineNumber)
        {
            return _originalLines[lineNumber];
        }
    }
}
